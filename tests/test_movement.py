from modules import movement

from math import sqrt


def test_find_distance():
    assert(movement.find_distance((0, 0), None) == float('inf'))
    assert(movement.find_distance((0, 0), (0, 0)) == 0.0)
    assert(movement.find_distance((0, 0), (5, 5)) == sqrt(50))

def test_find_closest():
    assert(movement.find_closest_point((0, 0), []) == None)
    assert(movement.find_closest_point((0, 0), [(2, 2), (4, 4)]) == (2 ,2))
    assert(movement.find_closest_point((0, 0), [(4, 4), (2, 2)]) == (2 ,2))
