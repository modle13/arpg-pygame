#!/usr/bin/env python

"""
adapted from https://realpython.com/pygame-a-primer
"""

from collections import Counter
import logging
import os
import pygame
import pygame_menu
import random

from arpg_pygame.camera import Camera
from arpg_pygame.colonists import Colonist
from arpg_pygame import colors
from arpg_pygame.control_panel import ControlPanel
from arpg_pygame.crt import CRT
from arpg_pygame.dynamic_text_group import DynamicTextGroup
from arpg_pygame.events import Events
from arpg_pygame import grid
from arpg_pygame.inventory import Inventory
from arpg_pygame import keys
from arpg_pygame.menu import Menu
from arpg_pygame.reticle import Reticle
from arpg_pygame.settings import config
from arpg_pygame.text import Text

logging.basicConfig(level=(logging.DEBUG if config.debug else logging.INFO),
    format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S')

logger = logging.getLogger(__name__)


class Game():
    def __init__(self):
        self.HERE = os.path.dirname(__file__)
        # Setup the clock for a decent framerate
        self.clock = pygame.time.Clock()

        # Create the screen object
        self.screen = pygame.display.set_mode((config.SCREEN_WIDTH, config.SCREEN_HEIGHT))

        # Create groups to hold sprites
        # - all_sprites is used for rendering
        self.all_sprites = pygame.sprite.Group()
        self.game_objects = pygame.sprite.Group()
        self.colonists = pygame.sprite.Group()
        self.grid_sprites = pygame.sprite.Group()
        self.products = pygame.sprite.Group()
        self.creatures = pygame.sprite.Group()
        self.shrooms = pygame.sprite.Group()
        self.trees = pygame.sprite.Group()
        self.wheat = pygame.sprite.Group()
        self.forager_targets = pygame.sprite.Group()
        self.farmer_targets = pygame.sprite.Group()
        self.porter_targets = pygame.sprite.Group()
        self.woodcutter_targets = pygame.sprite.Group()
        self.structures = pygame.sprite.Group()
        self.storage_structures = pygame.sprite.Group()
        self.blocking_sprites = pygame.sprite.Group()
        # for click detection on menu elements; used by events.py (move it to events.py?)
        self.menu_sprites = pygame.sprite.Group()
        # for update management control
        self.ui_containers = pygame.sprite.Group()
        # for use in toggling state of common elements via shared button
        self.context_menus = pygame.sprite.Group()
        self.start_menus = pygame.sprite.Group()
        # these don't update position relative to camera
        self.ui_sprites = pygame.sprite.Group()

        self.targets, self.resources = self.set_sprite_group_collections()

        # Just init this as the same as targets
        config.active_targets = self.targets.copy()

        # set up game state
        self.show_grid = True
        self.placement_mode_on = False
        self.placement_object = None
        self.colonists_active = True

        # generate initial sprites and other objects
        new_grid_sprites = grid.generate_squares()
        self.grid_sprites.add(new_grid_sprites)
        self.player = Reticle()
        config.camera = Camera(config.SCREEN_WIDTH / 2, config.SCREEN_HEIGHT / 2)
        self.events = Events()

        # set up ui elements
        self.inventory = Inventory(limit_type='unlimited', kinds=config.storage_items)
        self.controls_menu = Menu('controls', show=False)
        self.context_menus.add(self.controls_menu)
        self.ui_containers.add(self.controls_menu)

        # set initial input state vars
        self.mouse_buttons_state = (False, False, False)
        self.pressed_keys = pygame.key.get_pressed()
        self.make_main_menu()
        self.crt = CRT(config.SCREEN_WIDTH, config.SCREEN_HEIGHT)

        self.control_panel = ControlPanel(self)
        self.control_panel.inventory_display.contents = self.inventory.contents
        self.message_feed = self.control_panel.message_feed
        self.message_feed.add('colonization has begun...')

    def make_main_menu(self):
        self.menu = pygame_menu.Menu(400, 500, 'An Action RPG',
                       theme=pygame_menu.themes.THEME_DARK)
        self.menu.add_vertical_margin(30)
        self.menu.add_text_input('Character Name:', default='Numbers Go Up')
        self.menu.add_selector('Difficulty: ', [('Hard', 1), ('Easy', 2)], onchange=self.set_difficulty)
        self.menu.add_button('Play', self.start)
        self.menu.add_button('Quit', pygame_menu.events.EXIT)

    def set_difficulty(self, *args):
        print('difficulty set', *args)

    def run(self):
        # update state
        self.update()

        # draw sprites
        self.draw()

        # update the frame
        self.update_frame()

    def update(self):
        # update keys collection
        self.pressed_keys = pygame.key.get_pressed()

        # update events
        self.events.update()

        # update ui elements
        if not self.started or self.paused:
            self.menu.mainloop(self.screen)

        self.ui_containers.update()

        if not self.started or self.paused:
            return

        # update camera elements
        self.player.update()
        self.visible_area = pygame.Rect(
            self.player.rect.x - config.HALF_WIDTH,
            self.player.rect.y - config.HALF_HEIGHT,
            config.SCREEN_WIDTH,# - 100, # to test sprite draw area
            config.SCREEN_HEIGHT,# - 100, # to test sprite draw area
        )
        config.camera.update(self.player)

        # update ui elements
        self.control_panel.colonist_display.update()
        self.update_inventory_counts()
        self.control_panel.inventory_display.update()
        self.update_cursor()

        # Filter targets
        self.update_targets()
        # update sprites
        self.ui_sprites.update()
        self.grid_sprites.update()
        self.all_sprites.update()

    def update_targets(self):
        # maybe use global inventory object here
        # get all available capacity
        all_capacity = {}
        for structure in self.storage_structures:
            for item in config.storage_items:
                capacity = structure.output_inventory.get_capacity(item)
                current = all_capacity.get(item) or 0
                all_capacity[item] = current + capacity

        # filter for available targets
        for role in config.game.targets:
            config.active_targets[role] = list(
                filter(lambda x:
                    x.active
                    and not x.claimed,
                    config.game.targets.get(role)
                )
            )
            # filter again for available storage for porters
            if role == 'porter':
                config.active_targets[role] = list(
                    filter(lambda x:
                        (all_capacity.get(x.label) or 0) > 0,
                        config.active_targets.get(role)
                    )
                )

    def draw(self):
        # Fill the screen with black
        self.screen.fill(colors.BLACK)
        # Draw title and return if show_title
        if not self.started:
            for entity in self.start_menus:
                entity.draw()
            return
        # Draw the grid
        if self.show_grid:
            self.crt.draw(self.screen, 5)
        for entity in self.grid_sprites:
            if self.show_grid:
                self.blit(entity)
            #self.blit(entity.progress_display)
        # Draw all sprites
        for entity in self.all_sprites:
            self.blit(entity)
        # Draw entity context panes and reprs
        for entity in self.all_sprites:
            if hasattr(entity, 'debug_repr_sprite'):
                self.blit(entity.debug_repr_sprite)
            if hasattr(entity, 'context_pane'):
                self.blit(entity.context_pane, follow_camera=False)
            # Show the status messages on colonists
            if hasattr(entity, 'status_text_object'):
                self.blit(entity.status_text_object)
            # Show the next tile of colonists
            if hasattr(entity, 'target') and entity.target and entity.target.label == 'dummy' and config.debug:
                self.blit(entity.target)
            # Show the colonist path_tiles
            if hasattr(entity, 'path_tiles') and config.debug:
                for entry in entity.path_tiles:
                    self.blit(entry)
            if hasattr(entity, 'path_line_segments') and config.debug:
                # these do not follow the camera, but draw directly on the screen; need to offset with follow_camera=True
                entity.draw_line()
        # Draw the UI containers
        for entity in self.ui_containers:
            if entity.show:
                entity.draw()
        #if config.debug:
        # this is the temp player
        self.blit(self.player)

        # draw ui sprites
        for entity in self.ui_sprites:
            self.blit(entity, follow_camera=False)

        self.control_panel.draw(self.screen)

    def blit(self, sprite, follow_camera=True):
        if not sprite.show:
            return

        # execute additional draw function if present
        # this is used to draw objects related to target sprite
        if hasattr(sprite, 'draw'):
            sprite.draw(self.screen, camera=(config.camera if follow_camera else None))

        # draw the base sprite
        if follow_camera:
            if sprite.rect.colliderect(self.visible_area):
                self.screen.blit(sprite.surf, config.camera.apply(sprite))
        else:
            self.screen.blit(sprite.surf, sprite.rect)

    def update_frame(self):
        # makes the updated display frame visible
        pygame.display.flip()
        # resets the frame tick timer (milliseconds)
        self.clock.tick(30)
        #print(self.clock.get_fps())

    def start(self):
        self.menu.disable()
        for entity in self.start_menus:
            entity.show = False
        self.started = True
        self.paused = False
        # set initial structures (if this is done too soon the event gets swallowed)
        self.set_initial_structures()
        # initial event update
        self.events.update()

    def quit(self):
        pygame.mixer.music.stop()
        pygame.mixer.quit()

    def set_sprite_group_collections(self):
        targets = {
            'woodcutter': self.woodcutter_targets,
            'thresher': self.farmer_targets,
            'forager': self.forager_targets,
            'porter': self.porter_targets,
        }
        resources = {
            'creature': self.creatures,
            'shroom': self.shrooms,
            'tree': self.trees,
            'wheat': self.wheat,
        }
        # add dummy groups to avoid NoneType on target filters
        for structure_type in config.definitions.structures:
            if structure_type not in targets:
                targets[structure_type] = pygame.sprite.Group()
        return targets, resources

    def update_colonist_display_contents(self):
        # even though this function will override DynamicTextTroup.content_updater,
            # self will still refer to the game object
        try:
            # self.last_colonist_counts is used for comparisons against the current count
                # to determine whether update is needed
            last_counts = config.game.last_colonist_counts
        except AttributeError:
            last_counts = {}
        results = list(map(lambda x: x.role, config.game.colonists))
        counts = {x: results.count(x) for x in results}
        if counts != last_counts:
            logger.debug('ROLES COUNT CHANGED; updating display')
            all_roles = set().union(last_counts.keys(), counts.keys())
            for key in all_roles:
                value = counts.get(key) or 0
                logger.debug(f'setting {key}: {value}')
                self.control_panel.colonist_display.set(key, value)
            self.last_colonist_counts = counts.copy()

    def set_initial_structures(self):
        for structure in config.starting_structures:
            self.events.make_structure_event(**structure)

    def update_inventory_counts(self):
        # count all current storage items
        all_inventory = {}
        for entry in self.storage_structures:
            all_inventory = dict(Counter(all_inventory) + Counter(entry.output_inventory.contents))

        # update inventory contents dict
        #   set/get is needed fix issue of not updating entries missing from all_inventory
        #   which would leave displayed count at 1, when it was really 0
        for entry in self.inventory.contents:
            self.inventory.set(entry, all_inventory.get(entry) or 0)

    def update_cursor(self):
        mouse_visible = False if self.placement_object else True
        pygame.mouse.set_visible(mouse_visible)


def init_game():
    # Initialize pygame
    game = Game()

    # for use in other scripts
    config.game = game
    # Variable to keep the main loop running
    game.running = True
    game.started = False
    return game


if __name__ == '__main__':
    game = init_game()
    logger.info('game initialized')
    # Main loop
    while game.running:
        game.run()
    game.quit()
