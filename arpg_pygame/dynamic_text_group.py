import logging
import pygame

from arpg_pygame import colors
from arpg_pygame.panel_card import PanelCard
from arpg_pygame.settings import config
from arpg_pygame.text import Text

logger = logging.getLogger(__name__)


# extend Sprite to take advantage of sprite groups
class DynamicTextGroup(pygame.sprite.Sprite):
    def __init__(
            self,
            position=(0, 0),
            parent_sprite=None,
            offset=(0, 0),
            offset_mult=(0, 0),
            group_type='counter',
            label='unlabeled',
            show=True,
            content_updater=None,
            reversed_order=False,
            entry_limit=float('inf'),
            display_type='plain',
        ):
        # TODO: why init here?
        super(DynamicTextGroup, self).__init__()
        self.parent_sprite = parent_sprite
        self.label = label
        self.color_fg = colors.WHITE
        self.color_bg = colors.BLACK
        self.x, self.y = position
        self.x_offset, self.y_offset = offset
        self.x_offset_mult, self.y_offset_mult = offset_mult
        self.text_sprites = pygame.sprite.Group()
        if group_type == 'counter':
            self.contents = {}
            self.last_contents = {}
        else:
            self.contents = []
            self.last_contents = []
        self.show = show
        # for use in calculating x position based on entry size
        self.font = config.SMALLER_FONT
        # override content_updater function if param passed
        if content_updater:
            self.content_updater = content_updater
        self.reversed_order = reversed_order
        self.entry_limit = entry_limit
        self.display_type = display_type

    def update(self, force=False):
        self.set_text_sprite_visibility()
        self.content_updater()
        if not force and self.contents == self.last_contents:
            return
        config.game.menu_sprites.remove(self.text_sprites)
        logger.debug(f'{self.label} contents updated; recreating all the text elements')
        self.text_sprites.empty()

        for entry in self.contents:
            x_pos = self.x
            y_pos = self.y

            if self.parent_sprite:
                x_pos += self.parent_sprite.rect.x
                y_pos += self.parent_sprite.rect.y

            if isinstance(self.contents, dict):
                content = entry
                amount = self.contents.get(entry)
                text_contents = f'{entry}: {self.contents[entry]}'
            else:
                content = entry
                amount = 0
                text_contents = entry

            if self.display_type == 'card':
                text_sprite = self.draw_card(x_pos, y_pos, content, amount, len(self.text_sprites))
            else:
                text_sprite = self.draw_list(x_pos, y_pos, text_contents, len(self.text_sprites))

            self.text_sprites.add(text_sprite)

        config.game.menu_sprites.add(self.text_sprites)
        self.last_contents = self.contents.copy()

    def draw_card(self, x, y, content, amount, count):
        text_sprite = PanelCard(x=x, y=y, content=content, amount=amount, card_count=count)
        return text_sprite

    def draw_list(self, x, y, text_contents, count):
        font_width, _ = self.font.size(text_contents)
        self.x_offset = font_width if font_width > self.x_offset else self.x_offset
        x_pos = x + self.x_offset * self.x_offset_mult
        y_pos = y + self.y_offset * self.y_offset_mult
        # vary y_pos with number of sprites
        y_pos = y_pos + self.font.get_linesize() * count
        text_sprite = Text(self.font, self.color_fg, self.color_bg, x_pos, y_pos, label=text_contents, parent_sprite=self.parent_sprite)
        text_sprite.update(text_contents)
        return text_sprite

    def add(self, name, quantity=0):
        if isinstance(self.contents, dict):
            try:
                self.contents[name] += quantity
                logger.debug(f'added {quantity} {name} to {self.label}')
            except KeyError:
                self.contents[name] = quantity
        else:
            self.check_entry_limit()
            if self.reversed_order:
                self.contents.insert(0, name)
            else:
                self.contents.append(name)

    def check_entry_limit(self):
        if len(self.contents) > self.entry_limit:
            self.contents = self.contents[:self.entry_limit]

    def set(self, name, quantity):
        if isinstance(self.contents, dict):
            self.contents[name] = quantity or 0

    def set_all_text(self, entries: list):
        self.contents = entries

    def reset(self):
        if isinstance(self.contents, dict):
            self.contents = {}
        else:
            self.contents = []

    def toggle(self):
        self.show = not self.show
        for entry in self.text_sprites:
            entry.show = self.show

    def content_updater(self):
        pass

    def draw(self):
        for entry in self.text_sprites:
            config.game.screen.blit(entry.surf, entry.rect)

    def set_text_sprite_visibility(self):
        for entry in self.text_sprites:
            entry.show = self.show
