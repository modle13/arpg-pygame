from math import hypot
import pygame

from arpg_pygame.settings import config
from arpg_pygame.tile import Tile


def move(object_to_move):
    try:
        segments = object_to_move.path_line_segments
        target = segments[1]
        move_toward_target(object_to_move, target)
        return
        # temporary; reinstate below for multiple line segments
        first = segments[0]
        second = segments[1]
        xf, yf = first
        xs, ys = second
        distance = hypot(xs - xf, ys - yf)
        third = None
        if len(segments) > 2:
            third = segments[2]
        target = third if distance < 5 else second
        if not target:
            return
        move_toward_target(object_to_move, target)
    except IndexError:
        return


def move_toward_target(obj, target, target_type='normal'):
    # Move the sprite based on speed
    x_speed, y_speed = get_relative_speed(obj, target)
    if target_type == 'dummy':
        x_speed /= 2
        y_speed /= 2
    xo, yo = obj.rect.center
    xt, yt = target
    if xo < xt:
        obj.rect.move_ip(x_speed, 0)
    elif xo > xt:
        obj.rect.move_ip(-x_speed, 0)
    if yo < yt:
        obj.rect.move_ip(0, y_speed)
    elif yo > yt:
        obj.rect.move_ip(0, -y_speed)


# def move_toward_target(source, target, target_type='normal'):
#     # Move the sprite based on speed
#     x_speed, y_speed = get_relative_speed(source, target)
#     # If our target isn't real then just kind of saunter
#     if hasattr(source, 'resource_type'):
#         x_speed /= 4
#         y_speed /= 4
#     elif target_type == 'dummy':
#         x_speed /= 2
#         y_speed /= 2
#     if source.rect.center[0] < target.x:
#         source.rect.move_ip(x_speed, 0)
#     if source.rect.center[0] > target.x:
#         source.rect.move_ip(-x_speed, 0)
#     if source.rect.center[1] < target.y:
#         source.rect.move_ip(0, y_speed)
#     if source.rect.center[1] > target.y:
#         source.rect.move_ip(0, -y_speed)


def get_relative_speed(source, target):
    speed = config.movement_speed
    if hasattr(source, 'speed'):
        speed = source.speed

    # possibly better way to achieve this with use of hypot function
    # get position of self and target
    x_src, y_src = source.rect.center
    #x_tgt, y_tgt = target.center
    x_tgt, y_tgt = target
    # get the diffs
    x_diff = abs(x_src - x_tgt)
    y_diff = abs(y_src - y_tgt)
    # calculate the speed ratio
    x_ratio = 1
    y_ratio = 1
    if x_diff and y_diff and x_diff != y_diff:
        y_ratio = y_diff / x_diff
        # x ratio is just the inverse of y ratio
        x_ratio = 1 / y_ratio

    # calculate the speed
    y_speed = speed if y_ratio > 1 else y_ratio * speed
    x_speed = speed if x_ratio > 1 else x_ratio * speed
    return x_speed, y_speed


def calculate_path(entity, target):
    # this would be used for avoiding collisions on obstacles
    # ignoring this for now
    return
    unit  = config.tile_size
    # reduce the amount of iterations by dividing by tile unit size
    start_x, start_y  = (entity.rect.center[0] // unit, entity.rect.center[1] // unit)
    end_x  , end_y    = (target.rect.center[0] // unit, target.rect.center[1] // unit)
    sign_x = 1 if start_x - end_x <= 0 else -1
    sign_y = 1 if start_y - end_y <= 0 else -1
    path = []

    # set the path
    # range is not end inclusive, so we have to adjust slightly
    # by adding the increasing or decreasing depending on directional
    # to avoid being too far away from target when working it
    for x in range(start_x, end_x + sign_x, sign_x):
        path.append((x    , start_y, 'horizontal', sign_x))
    for y in range(start_y, end_y + sign_y, sign_y):
        path.append((end_x, y      , 'vertical'  , sign_y))
    create_all_path_tiles(path, entity)


def create_all_path_tiles(path, entity):
    # could create these automatically during path generation
    # then kill each one as it is reached
    unit = config.tile_size
    half_unit = unit / 2
    entity.path_tiles = []
    accrued_offset = {'x': 0, 'y': 0}
    for entry in path:
        blocked = True
        count = 0
        # keep track of total offsets accrued
        x, y, axis, direction = entry
        while blocked and count < 20:
            if count > 0:
                print('still blocked')
            # current tile offsets will always be 0
            # unless next tile collides with an occupied space
            offset = count * unit
            x_offset = offset if axis == 'vertical'   else 0
            y_offset = offset if axis == 'horizontal' else 0
            tile_position = (
                x * unit + half_unit + (x_offset + accrued_offset['x']) * direction,
                y * unit + half_unit + (y_offset + accrued_offset['y']) * direction
            )
            new_tile_marker = Tile(tile_position, entity.default_color)
            blocked = check_if_blocked(new_tile_marker)
            count += 1
        if blocked:
            # did not find a path, so give up
            continue
        elif count:
            # adjust accrued_offset with the current tile's offset count
            accrued_offset['x'] += x_offset
            accrued_offset['y'] += y_offset


        entity.path_tiles.append(new_tile_marker)
    last_tile = Tile(entity.target.rect.center, entity.default_color)
    entity.path_tiles.append(last_tile)


def check_if_blocked(sprite):
    return pygame.sprite.spritecollideany(sprite, config.game.blocking_sprites)


def get_next_tile(entity):
    unit = config.tile_size
    if not entity.next_tile and entity.path_tiles:
        entity.next_tile = entity.path_tiles.pop(0) #next((x for x in self.path), None)


def check_target_nearby(entity, target):
    return False
    target_x, target_y = target.rect.center
    distance = hypot(target_x - entity.rect.x, target_y - entity.rect.y)
    if distance < config.tile_size * target.target_size:
        return True
    return False


def find_closest_point(initial_point: tuple, list_of_points):
    closest = float('inf')
    min_point = None
    if list_of_points == []:
        return None
    for point in list_of_points:
        distance = find_distance(initial_point, point)
        if distance < closest:
            min_point = point
            closest = distance
    assert(min_point is not None)
    return min_point


def find_distance(point1: tuple, point2: tuple):
    try:
        x1, y1 = point1
        x2, y2 = point2
    except TypeError:
        # float('inf') is an unbounded upper value (i.e. maxest max)
        return float('inf')
    return hypot(x2 - x1, y2 - y1)


def get_closest_target(entity, available):
    found = None
    target_point = find_closest_point(entity.rect.center, list(map(
        lambda x: x.rect.center, available
        )))
    if target_point is not None:
        for sprite in available:
            if sprite.rect.center == target_point:
                return sprite

    return None


def check_reached_target(entity):
    # still need this to handle collecting items from storage for processing
    collided_target = entity.next_tile and entity.target and pygame.sprite.collide_rect(entity.next_tile, entity.target)
    if collided_target:
        if entity.target.target_type == 'dummy':
            entity.trigger_state_reset()
        return

    # will the next tile collide with target?
    collided_next = None if not entity.next_tile else pygame.sprite.collide_rect(entity, entity.next_tile)
    if collided_next:
        entity.next_tile = None

