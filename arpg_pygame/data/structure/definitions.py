from arpg_pygame import colors

# polygon sizes should always be calculated as fractions;
#   scaling will occur based on unit size and structure size during instantiation
#   this avoids having to care about grid size and scaling at this definition time

# TODO: should these follow clockwise? topleft > topright > bottomright > bottomleft
default_polygon = [
    # bottom (left to right)
    (0, 1),
    (1 / 5, 1),
    (2 / 5, 3 / 4),
    (3 / 5, 3 / 4),
    (4 / 5, 1),
    (1, 1),
    # right (bottom to top)
    (3 / 4, 1 / 4),
    # top (right to left)
    (1 / 4, 1 / 4),
    # left (top to bottom) automatically drawn with polygon completion
]

octagon_polygon = [
    # top (left to right)
    (1 / 3, 0),
    (2 / 3, 0),
    # right (top to bottom)
    (1, 1 / 3),
    (1, 2 / 3),
    # bottom (right to left)
    (2 / 3, 1),
    (1 / 3, 1),
    # left (bottom to top)
    (0, 2 / 3),
    (0, 1 / 3),
]

# default sizes will be (1, 1); set `'size': sizes.get('size_you_want'),` key on a structure type to set a custom size
sizes = {
    'std': (2, 2),
    'wide': (3, 2),
    'narrow': (1, 2),
    'narrow_rotated': (2, 1),
    'std_3': (3, 3),
}


"""
possible fields; schema-validator would be useful here

# type
target_type       : string, currently everything is type 'storage';
                            this should be defaulted True instead of a string
action_archetype  : string, colonist action archetype to be assigned

# flags
buildable         : bool,   structure can be built via structure selection menu
impervious        : bool,   structure will ignore delete/destroy commands
operable          : bool,   a colonist can be assigned
show_influence    : bool,   toggles influence range visibility
workable          : bool,   is a workable target
                                assigned colonist will work to produce items

# products
capacity          : int,    max number for each type of inventory
cultivate_data    : dict,   data used for cultivation, include item and event name
                                example: {'name': 'wheat_seed', 'event_id': 'ADDWHEAT'},
input_types       : list,   items to be processed
production_map    : dict,   defines production details for target items
                                example: {'fertilizer': {'consumes': {'compost': 1}, 'produces': 1}},
                                this consumes 1 compost and produces one fertilizer
                                add more keys to consumes dictionary to change requirements
storage_types     : list,   items eligible to be stored in the output

# workers
helper_max        : int,    max number of additional helpers that can be assigned
influence_range   : int,    number of tiles from center to edge of
                                influence square around structure

# draw
color             : const,  a 3-element tuple imported from the colors module
polygon           : list,   tuples, points of a polygon
size              : string, key reference to sizes dict
"""
structures = {
    'composter': {
        'target_type': 'storage',
        'action_archetype': 'processor',
        'operable': True,
        'show_influence': False,
        'workable': True,
        'capacity': 10,
        'input_types': ['compost'],
        'production_map': {'fertilizer': {'consumes': {'compost': 2}, 'produces': 1}},
        'storage_types': ['fertilizer'],
        'helper_max': 5,
        'influence_range': 1,
        'color': colors.ORANGE,
        'polygon': default_polygon,
    },
    'wheat_farmer': {
        'target_type': 'storage',
        'action_archetype': 'cultivator',
        'show_influence': True,
        'operable': True,
        'workable': True,
        'capacity': 10,
        'cultivate_data': {'name': 'wheat_seed', 'event_id': 'ADDWHEAT'},
        'input_types': ['fertilizer', 'wheat_germ'],
        'production_map': {'wheat_seed': {'consumes': {'wheat_germ': 1, 'fertilizer': 1}, 'produces': 1}},
        'storage_types': ['wheat_seed'],
        'helper_max': 5,
        'influence_range': 7,
        'color': colors.YELLOW,
        'polygon': default_polygon,
    },
    'forester': {
        'target_type': 'storage',
        'action_archetype': 'cultivator',
        'show_influence': True,
        'operable': True,
        'workable': True,
        'capacity': 10,
        'cultivate_data': {'name': 'sapling', 'event_id': 'ADDTREE'},
        'input_types': ['fertilizer'],
        'production_map': {'sapling': {'consumes': {'fertilizer': 1}, 'produces': 1}},
        'storage_types': ['sapling'],
        'helper_max': 5,
        'influence_range': 7,
        'color': colors.LIGHT_GREEN,
        'polygon': default_polygon,
    },
    'sawmill': {
        'target_type': 'storage',
        'action_archetype': 'processor',
        'operable': True,
        'show_influence': False,
        'workable': True,
        'capacity': 10,
        'input_types': ['log'],
        'production_map': {'plank': {'consumes': {'log': 1}, 'produces': {'plank': 2, 'sawdust': 1}}},
        'storage_types': ['plank', 'sawdust'],
        'helper_max': 5,
        'influence_range': 1,
        'color': colors.YELLOW,
        'polygon': default_polygon,
    },
    'kitchen': {
        'target_type': 'storage',
        'action_archetype': 'processor',
        'operable': True,
        'show_influence': False,
        'workable': True,
        'capacity': 10,
        'input_types': ['shroombit', 'grain'],
        'production_map': {'soup': {'consumes': {'shroombit': 1, 'grain': 1}, 'produces': 1}},
        'storage_types': ['soup'],
        'helper_max': 5,
        'influence_range': 1,
        'color': colors.BROWN,
        'polygon': default_polygon,
    },
    'hunter': {
        'target_type': 'storage',
        'action_archetype': 'harvester',
        'operable': True,
        'helper_max': 5,
        'capacity': 10,
        'storage_types': ['meat', 'hide', 'bone', 'fat'],
        'color': colors.TAN,
        'polygon': default_polygon,
    },
    'thresher': {
        'target_type': 'storage',
        'action_archetype': 'harvester',
        'operable': True,
        'show_influence': True,
        'helper_max': 5,
        'influence_range': 8,
        'capacity': 10,
        'storage_types': ['straw', 'grain', 'wheat_germ', 'compost'],
        'color': colors.LIGHT_YELLOW,
        'polygon': default_polygon,
    },
    'woodcutter': {
        'target_type': 'storage',
        'action_archetype': 'harvester',
        'operable': True,
        'show_influence': True,
        'capacity': 10,
        'storage_types': ['log', 'compost'],
        'helper_max': 5,
        'influence_range': 8,
        'color': colors.GRAY,
        'polygon': default_polygon,
    },
    'forager': {
        'target_type': 'storage',
        'action_archetype': 'harvester',
        'harvester': True,
        'operable': True,
        'show_influence': True,
        'helper_max': 5,
        'influence_range': 8,
        'capacity': 10,
        'storage_types': ['shroombit', 'compost'],
        'color': colors.BROWN,
        'polygon': default_polygon,
    },
    'town_center': {
        'target_type': 'storage',
        'buildable': False,
        'impervious': True,
        'operable': False,
        'capacity': 1000,
        # storage_types are set dynamically below
        'storage_types': [],
        'helper_max': 0,
        'color': colors.PINK,
        'polygon': octagon_polygon,
        #'size': sizes.get('std_3'),
    },
}

# aggregate storage types
storage_items = list(map(
        lambda elem: elem[1].get('storage_types'),
        structures.items()
    ))
# flatten storage_types list of lists
storage_items = [item for sublist in storage_items for item in sublist if item]
# get unique list of storage types
storage_items = list(set(storage_items))
# set town_center storage_types
structures.get('town_center')['storage_types'] = storage_items
