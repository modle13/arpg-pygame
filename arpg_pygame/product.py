import pygame

from arpg_pygame import colors
from arpg_pygame.context_pane import ContextPane
from arpg_pygame.settings import config

class Product(pygame.sprite.Sprite):
    def __init__(self, position, product_type):
        super(Product, self).__init__()
        self.start = position
        self.product_type = product_type
        self.label = product_type
        self.surf = self.get_type_surface(self.product_type)
        self.rect = self.surf.get_rect(center=self.start)
        self.sprite_type = 'product'
        self.target_type = 'product'
        self.active = True
        self.claimed = False
        self.expired = False
        self.show = True
        self.set_reprs()
        self.context_pane = ContextPane(self, self.sprite_type)
        self.target_size = 1

    def get_type_surface(self, product_type):
        dimensions = (3, 3)
        color = colors.YELLOW

        if product_type == 'log':
            dimensions = (10, 5)
            color = colors.LIGHT_BROWN
        elif product_type == 'shroom':
            dimensions = (4, 4)
            color = colors.OFF_WHITE

        surf = pygame.Surface(dimensions)
        surf.fill(color)
        return surf

    def update(self):
        self.context_pane.update()

    def work(self):
        # no work needed here, just collecting
        return True

    def destroy(self):
        self.expired = True
        self.produce()
        self.kill()

    def produce(self):
        pass

    def show_context(self):
        self.context_pane.display()

    def label_repr(self):
        return self.label

    def position_repr(self):
        return self.rect.center

    def claimed_repr(self):
        return "TRUE" if self.claimed else "FALSE"

    def set_reprs(self):
        self.reprs = {
            'label': self.label_repr,
            'position': self.position_repr,
            'claimed': self.claimed_repr,
        }
