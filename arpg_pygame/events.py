import logging
import pygame
import random

from arpg_pygame.colonists import Colonist
from arpg_pygame import grid
from arpg_pygame import keys
from arpg_pygame.messages import Message
from arpg_pygame.resource import Resource
from arpg_pygame.screenshots import save_screenshot
from arpg_pygame.settings import config
from arpg_pygame.structure import Structure


logger = logging.getLogger(__name__)

class Events():
    def __init__(self):
        # Create a custom event for adding a new objects
        # this is just an int, essentially a custom id
        self.ADDCOLONIST = pygame.USEREVENT + 1
        pygame.time.set_timer(self.ADDCOLONIST, 500)
        self.ADDPRODUCT = pygame.USEREVENT + 2
        self.ADDTREE = pygame.USEREVENT + 3
        pygame.time.set_timer(self.ADDTREE, 250)
        self.ADDSHROOM = pygame.USEREVENT + 4
        pygame.time.set_timer(self.ADDSHROOM, 250)
        self.ADDWHEAT = pygame.USEREVENT + 5
        pygame.time.set_timer(self.ADDWHEAT, 250)
        self.ADDCREATURE = pygame.USEREVENT + 100
        #pygame.time.set_timer(self.ADDCREATURE, 250)
        self.right_mouse_drag = False
        self.mouse_pos = (0, 0)
        self.ADDSTRUCTURE = pygame.USEREVENT + 200


    def update(self):
        # Look at every event in the queue
        events = pygame.event.get()
        for event in events:
            # start with game flow events
            if event.type == keys.KEYDOWN and event.key == keys.K_RETURN and not config.game.started:
                config.game.start()
                break
            # exit game
            elif event.type == keys.KEYDOWN and event.key == keys.K_ESCAPE and config.game.pressed_keys[keys.K_LALT]:
                config.game.running = False
            elif not config.game.started:
                break
            # show the menu
            elif event.type == keys.KEYDOWN and event.key == keys.K_ESCAPE:
                config.game.paused = False
                had_active_ui_element = self.reset_placement_mode()
                had_active_ui_element = had_active_ui_element or self.hide_sprite_context_panes(had_active_ui_element)
                had_active_ui_element = had_active_ui_element or self.hide_menus(had_active_ui_element)
                if not had_active_ui_element:
                    config.game.controls_menu.toggle()
                    config.game.paused = True
            elif config.game.paused:
                return

            elif event.type == keys.KEYUP and event.key == keys.K_LSHIFT:
                self.reset_placement_mode()

            # Check mouse events
            elif event.type == keys.MOUSEBUTTONDOWN and event.button == 1:
                self.handle_left_click(event)
            # Deactivate or delete structure if right-clicked
            elif event.type == keys.MOUSEBUTTONDOWN and event.button == 3:
                self.handle_right_click(event)

            elif event.type == keys.MOUSEBUTTONUP and event.button == 3:
                pygame.event.set_grab(False)
                config.game.reticle.clear_mouse_offset()
                self.right_mouse_drag = False

            # Process gameplay key events
            # did the user let go of the LSHIFT key?
            elif event.type == keys.KEYUP and event.key == keys.K_LSHIFT:
                config.game.placement_mode_on = False
                config.game.placement_object = None
            # take screenshot?
            elif event.type == keys.KEYDOWN and event.key == keys.K_F2:
                save_screenshot()
            # toggle debug?
            elif event.type == keys.KEYDOWN and event.key == keys.K_F3:
                config.debug = not config.debug
                logging.getLogger().setLevel(logging.DEBUG if config.debug else logging.INFO)
                logger.info(f'\n\nlogging debug {"ENABLED" if config.debug else "DISABLED"}\n')
            # toggle colonist movement?
            elif event.type == keys.KEYDOWN and event.key == keys.K_F4:
                config.game.colonists_active = not config.game.colonists_active
            # toggle grid?
            elif event.type == keys.KEYDOWN and event.key == keys.K_F5:
                config.game.show_grid = not config.game.show_grid
            # toggle inventory counter?
            elif event.type == keys.KEYDOWN and event.key == keys.K_i:
                config.game.control_panel.toggle_stores()
            # toggle structure list?
            elif event.type == keys.KEYDOWN and event.key == keys.K_b:
                config.game.control_panel.toggle_structures()
            # toggle colonist counter?
            elif event.type == keys.KEYDOWN and event.key == keys.K_c:
                config.game.control_panel.toggle_colonists()
            # toggle message feed?
            elif event.type == keys.KEYDOWN and event.key == keys.K_l:
                config.game.message_feed.toggle()
            # Did the user click the window close button? If so, stop the loop.
            elif event.type == keys.QUIT:
                config.game.running = False

            # process sprite add events
            # Add a new colonist?
            elif event.type == self.ADDCOLONIST:
                if len(config.game.colonists) < config.max_colonists:
                    new_colonist = Colonist()
                    config.game.colonists.add(new_colonist)
                    config.game.game_objects.add(new_colonist)
                    config.game.all_sprites.add(new_colonist)

            # Add a new creature?
            elif event.type == self.ADDCREATURE:
                self.add_new_resource(event, 'ADDCREATURE')

            # Add a new shroom?
            elif event.type == self.ADDSHROOM:
                self.add_new_resource(event, 'ADDSHROOM')

            # Add a new tree?
            elif event.type == self.ADDTREE:
                self.add_new_resource(event, 'ADDTREE')

            # Add a new wheat?
            elif event.type == self.ADDWHEAT:
                self.add_new_resource(event, 'ADDWHEAT')

            # Add a product?
            elif event.type == self.ADDPRODUCT:
                sprites_to_add = []
                while len(sprites_to_add) < event.quantity:
                    x = random.randrange(event.position[0] - 10, event.position[0] + 10)
                    y = random.randrange(event.position[1] - 10, event.position[1] + 10)
                    new_pos = (x, y)
                    sprite = event.product(new_pos, event.product_type)
                    sprites_to_add.append(sprite)
                config.game.products.add(sprites_to_add)
                config.game.targets.get('porter').add(sprites_to_add)
                config.game.all_sprites.add(sprites_to_add)
                config.game.game_objects.add(sprites_to_add)

            # Add a structure:
            elif event.type == self.ADDSTRUCTURE:
                new_structure = Structure(event.position, event.kind)
                config.game.structures.add(new_structure)
                config.game.blocking_sprites.add(new_structure)
                config.game.all_sprites.add(new_structure)
                config.game.game_objects.add(new_structure)
                if event.kind in config.storage_structures:
                    config.game.storage_structures.add(new_structure)

    def add_new_resource(self, event, event_type):
        event_config = config.resource_events.get(event_type)
        position = None
        if hasattr(event, 'position'):
            position = event.position

        name = event_config.get('name')
        resource_group = config.game.resources.get(name)
        if not position and len(resource_group) < event_config.get('max'):
            random_sprite = grid.get_random_grid_square()
            if not random_sprite:
                # no space available
                return
            position = random_sprite.rect.center
        if not position:
            return

        new_resource = Resource(position, name)
        resource_group.add(new_resource)
        config.game.blocking_sprites.add(new_resource)
        role = event_config.get('role')
        config.game.targets.get(role).add(new_resource)
        config.game.all_sprites.add(new_resource)
        config.game.game_objects.add(new_resource)

    def handle_left_click(self, event):
        # is this a UI menu click?
        ui_target = self.check_for_menu_click(event.pos)
        sprite_target = self.check_for_sprite_click(event.pos)
        if config.game.placement_mode_on:
            logging.debug('placement mode is on')
            target_square = grid.get_collide_grid_square(event.pos)
            if not target_square:
                logger.error(f'location {event.pos} is occupied; not placing {config.game.placement_object.structure_type}')
                return
            # TODO: reverse the order of these params to match Structure object
            self.make_structure_event(kind=config.game.placement_object.structure_type, position=target_square.rect.topleft)
            logger.info(f'making a {config.game.placement_object.structure_type} at {target_square.rect.topleft}')
            # enables multi-press mode
            if not config.game.pressed_keys[keys.K_LSHIFT]:
                self.reset_placement_mode()
        elif ui_target:
            if config.game.placement_object:
                self.reset_placement_mode()
            config.game.placement_mode_on = True
            config.game.placement_object = Structure(event.pos, ui_target.label, cursor=True)
            config.game.ui_sprites.add(config.game.placement_object)
            # what is this line doing? releasing reference to object? why set it to that object's label?
            ui_target = ui_target.label
        elif sprite_target:
            self.hide_sprite_context_panes(False)
            sprite_target.show_context()

    def handle_right_click(self, event):
        """Toggles/deletes structures; moves reticle/player."""
        # check structures
        for entry in config.game.structures:
            if config.camera.apply(entry).collidepoint(event.pos):
                if config.game.pressed_keys[keys.K_DELETE]:
                    entry.destroy()
                else:
                    entry.toggle()
                return

        # if structure not interacted with, handle movement
        pygame.event.set_grab(True)
        config.game.reticle.set_mouse_offset(event.pos)
        self.right_mouse_drag = True
        self.down_mouse_pos = event.pos

    def check_for_menu_click(self, position):
        for entry in config.game.menu_sprites:
            if entry.show and entry.rect.collidepoint(position):
                if entry.label in config.definitions.structures:
                    logging.info(f'found a placeable: {entry.label}')
                    return entry
        return None

    def check_for_sprite_click(self, position):
        for entry in config.game.game_objects:
            if entry.show and config.camera.apply(entry).collidepoint(position):
                logger.info(f'player clicked on: {entry.label}')
                return entry
        return None

    def reset_placement_mode(self):
        deactivated = False
        if config.game.placement_object:
            deactivated = True
            config.game.placement_mode_on = False
            # need to kill to remove sprite from sprite list
            config.game.placement_object.destroy()
            # need to dereference to cause mouse to reappear
            config.game.placement_object = None
        return deactivated

    def make_structure_event(self, kind=None, position=None):
        if not kind or not position:
            logger.error(f'not sure what this structure is or where to put it: kind={kind}, position={position}')
        make_thing = pygame.event.Event(self.ADDSTRUCTURE, kind=kind, position=position)
        # post event to event queue
        pygame.event.post(make_thing)

    def hide_menus(self, had_active):
        for sprite in config.game.context_menus:
            if sprite.show:
                logger.info(f'closing {sprite.label} menu')
                had_active = True
                sprite.toggle()
        return had_active

    def hide_sprite_context_panes(self, had_active):
        for sprite in config.game.game_objects:
            if sprite.context_pane.show:
                logger.info(f'closing {sprite.label} menu')
                sprite.context_pane.hide()
                had_active = True
        return had_active
