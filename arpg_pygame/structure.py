from collections import Counter
import logging
import pygame
import random
import time

from arpg_pygame import colors
from arpg_pygame.context_pane import ContextPane
from arpg_pygame.disabled import Disabled
from arpg_pygame import grid
from arpg_pygame.inventory import Inventory
from arpg_pygame.product import Product
from arpg_pygame.progress import Progress
from arpg_pygame.settings import config
from arpg_pygame.text import Text

logger = logging.getLogger(__name__)

class Influence(pygame.sprite.Sprite):
    def __init__(self, position, influence_range, show=True):
        self.position = position
        self.range = influence_range
        self.show = show
        if not self.range % 2: # then it's even
            self.range += 1 # but it needs to be odd for symmetry
        self.surf, self.rect = self.generate_sprite()

    def generate_sprite(self):
        unit = config.grid_square_size * self.range
        size = (unit, unit)
        # build transparent surface
        surf = pygame.Surface(size, pygame.SRCALPHA)
        rect = surf.get_rect(center=(self.position))
        # add alpha
        alpha = 64
        border_color = colors.GRAY
        fill_color = (*colors.LIGHT_GREEN, alpha)
        surf.fill(border_color)
        surf.fill(fill_color, surf.get_rect().inflate(-10, -10))
        return surf, rect

    def set_pos(self, pos):
        unit = config.grid_square_size
        self.rect.center = (pos[0] + unit / 2, pos[1] + unit / 2)


class Structure(pygame.sprite.Sprite):
    def __init__(self, position, structure_type, cursor=False):
        super(Structure, self).__init__()
        # TODO: This needs to be reorganized; some params belong in non-cursor logic
        # order should be: critical vars on top, like active, name
        # then alphabetical
        self.position = position
        self.structure_type = structure_type
        self.label = structure_type
        self.sprite_type = 'structure'
        self.cursor = cursor
        self.show = True
        self.surf, self.rect = self.generate_surface()
        structure_def = config.definitions.structures.get(self.structure_type)
        self.impervious = structure_def.get('impervious') or False
        self.operator = None
        # helpers; increase production rate on a curve like 1.25, 1.375, 1.45, etc.; increase smaller with each new colonist
        self.helpers = pygame.sprite.Group()
        self.debug_repr_sprite = Text(config.TINY_FONT, colors.WHITE, colors.TRANSPARENT, 0, -5)
        self.debug_repr_sprite.update(structure_type)
        self.influence = Influence(position, structure_def.get('influence_range') or 1, show=structure_def.get('show_influence'))
        self.progress = 0
        self.disabled_display = Disabled(self)
        self.progress_display = Progress(self)
        if not self.cursor: # then it's a real structure so handle assignments and inventory
            self.active = True
            self.helper_max = structure_def.get('max_helpers') or 0
            self.operable = structure_def.get('operable') or False
            self.target_type = structure_def.get('target_type') or ''
            self.workable = structure_def.get('workable') or False
            self.cultivate_data = structure_def.get('cultivate_data') or {}
            self.target_size, _ = structure_def.get('size') or (1, 1)
            self.action_archetype = structure_def.get('action_archetype') or ''
            self.ready = False
            self.output_available = False
            if self.operable:
                # primary operator; will always have an operator if active
                self.assign_operator()
            self.claimed = False
            self.input_types = structure_def.get('input_types') or []
            self.storage_types = structure_def.get('storage_types') or []
            self.output_inventory = Inventory(limit=structure_def.get('capacity') or 0, kinds=self.storage_types)
            if self.input_types:
                self.input_inventory = Inventory(limit=structure_def.get('capacity') or 0, purpose='input', kinds=self.input_types)
            self.input_ready = False
            self.input_full = False
            self.output_full = False
            self.full = False
            self.production_map = structure_def.get('production_map') or {}
            self.product_selected = None if not self.production_map else list(self.production_map.keys()).pop()
            self.set_reprs()
            self.complete_threshold = 100
            self.context_pane = ContextPane(self, self.sprite_type)
            initial_inventory = config.initial_inventory.get(self.label)
            if initial_inventory:
                self.store(initial_inventory)

    def generate_surface(self):
        details = config.definitions.structures.get(self.structure_type)
        size = details.get('size') or (1, 1)
        surf_size = tuple(config.grid_square_size * x for x in size)
        polygon_points = details.get('polygon')
        # make transparent surface to draw polygon onto
        # SRCALPHA makes it transparent; this is the surface the polygon will get drawn on
        surf = pygame.Surface(surf_size, pygame.SRCALPHA)
        # scale the polygon_points
        points = list(map(lambda x: (x[0] * surf_size[0], x[1] * surf_size[1]), polygon_points))
        pygame.draw.polygon(surf, details.get('color'), points)
        rect = surf.get_rect(topleft=self.position)
        # want to return rather than setting via self dot-notation so we can clearly see the params in init
        return surf, rect

    def work(self):
        if self.workable and self.ready:
            self.progress += 2
            completed = self.progress >= self.complete_threshold
            if completed:
                self.progress -= self.complete_threshold
                self.process_work_complete()
            return completed
        else:
            return True

    def process_work_complete(self):
        self.ready = False
        product_details = self.production_map.get(self.product_selected)
        consumed = product_details.get('consumes')
        for key_consumed, value_consumed in consumed.items():
            self.input_inventory.remove(key_consumed, value_consumed)
        produced = product_details.get('produces')
        if type(produced) == dict:
            for key_produced, value_produced in produced.items():
                self.output_inventory.add(key_produced, value_produced)
        elif type(produced) == int:
            self.output_inventory.add(self.product_selected, produced)
        else:
            logger.error(f'invalid data type detected in structure produces field; expected int or dict, got {type(produced)}')

    def store(self, items: dict):
        types = self.storage_types
        inventory = self.output_inventory
        if self.workable:
            types = self.input_types
            inventory = self.input_inventory
        for key, value in items.items():
            if key in types and value:
                added = inventory.add(key, value)
                items[key] = value - added

    def retrieve(self, item, needed_quantity):
        return_amount = self.output_inventory.remove(item, needed_quantity)
        return return_amount

    def update(self):
        self.check_inventory()
        text = self.structure_type
        unit = config.grid_square_size
        size = (self.rect.width, self.rect.height)
        # structures can be cursor objects, which replace the cursor with the structure
        # these structures should not perform standard structure operations
        if self.cursor:
            mouse_pos = pygame.mouse.get_pos()
            pos = grid.snap_to_grid(mouse_pos)
            self.rect.topleft = pos
        else:
            if not self.active:
                text = f'{text}: INACTIVE'
            pos = self.rect.topleft
            if self.active:
                if not self.operator and self.operable:
                    self.assign_operator()
            elif not self.active and (self.operator or self.helpers):
                self.unassign_all_colonists()
        repr_pos = (pos[0] + self.rect.width / 2, pos[1] - self.debug_repr_sprite.font.get_linesize())
        self.debug_repr_sprite.update(text)
        self.debug_repr_sprite.set_pos(repr_pos)
        self.influence.set_pos(pos)
        if not self.cursor:
            self.context_pane.update()
            self.progress_display.update(self.progress)

    def draw(self, screen, camera=None):
        # double-self intentional here because the structure objects directly contains its surf/rect
        self.blit(self, screen, camera=camera)
        self.blit(self.disabled_display, screen, camera=camera)
        self.blit(self.influence, screen, camera=camera)
        self.blit(self.debug_repr_sprite, screen, camera=camera)
        self.blit(self.progress_display, screen, camera=camera)

    def blit(self, obj, screen, camera=None):
        if not obj.show:
            return
        screen.blit(obj.surf, obj.rect if not camera else camera.apply(obj))

    def check_inventory(self):
        if self.cursor:
            return

        self.check_output_inventory()

        if self.workable:
            self.input_full = self.input_inventory.is_full()
            needed = self.production_map.get(self.product_selected).get('consumes')
            self.input_ready = True
            for key, value in needed.items():
                self.input_ready = self.input_ready and self.input_inventory.has_amount(key, value)
        self.ready = False
        if self.workable and self.input_ready and not self.output_full:
            self.ready = True
        self.output_available = not self.output_inventory.is_empty()
        self.full = self.output_full and self.input_full

    def check_output_inventory(self) -> None:
        if not self.product_selected:
            # then this is not a producing structure
            self.output_full = False
            return

        self.output_full = False
        product_details = self.production_map.get(self.product_selected)
        produces = product_details.get('produces')
        if type(produces) == dict:
            # structure has byproducts in addition to primary product
            # so need to loop and check to see if any are full
            # and set output_full for any
            for key, value in produces.items():
                self.output_full = self.output_inventory.is_full(kind=key)
                if self.output_full:
                    # stop checking
                    return
        elif type(produces) == int:
            # then just check product selected because there's no byproduct
            self.output_full = self.output_inventory.is_full(kind=self.product_selected)

    def check_inventory_for(self, kind):
        has_kind = self.output_inventory.contains(kind)
        return has_kind

    def toggle(self):
        self.active = not self.active
        self.disabled_display.show = not self.active
        logger.info(f'{self.structure_type} active? {self.active}; operator: {self.operator}; helpers: {self.helpers}')
        logger.info(f'show disabled? {self.disabled_display.show}')

    def activate(self):
        self.active = True

    def deactivate(self):
        if self.impervious:
            return
        self.active = False

    def unassign_all_colonists(self):
        if self.operator:
            # relieve operator from duty
            self.operator.unassign_role()
            # disassociate operator colonist from structure
            self.operator = None
        for helper in self.helpers:
            if helper:
                helper.unassign_role()
                self.helpers.remove(helper)

    def assign_helper(self):
        helper = self.get_colonist()
        if helper:
            self.helpers.add(helper)

    def assign_operator(self):
        self.operator = self.get_colonist()
        if self.operator:
            self.operator.assign(self)

    def get_colonist(self):
        # get next unassigned colonist (non-default)
        colonist = next((x for x in config.game.colonists if x.role == config.colonist_def.get('default_role')), None)
        if colonist:
            colonist.role = self.structure_type
            colonist.action_archetype = self.action_archetype
        return colonist

    def destroy(self):
        if self.impervious:
            return
        self.unassign_all_colonists()
        self.influence_surf = None
        self.drop_all_inventory()
        self.kill()

    def drop_all_inventory(self):
        if self.cursor:
            return
        input_inventory = {} if not hasattr(self, 'input_inventory') else self.input_inventory.contents
        all_inventory = dict(Counter(self.output_inventory.contents) + Counter(input_inventory))
        for key, value in all_inventory.items():
            make_thing = pygame.event.Event(config.game.events.ADDPRODUCT, product=Product, product_type=key, position=self.position, quantity=value)
            # post event to event queue
            pygame.event.post(make_thing)

    def show_context(self):
        self.context_pane.display()

    def inventory_repr(self):
        return self.output_inventory.get_contents_repr()

    def input_inventory_repr(self):
        return self.input_inventory.get_contents_repr()

    def label_repr(self):
        return self.label

    def position_repr(self):
        return self.rect.center

    def workable_repr(self):
        workable = 'NO'
        if self.ready:
            workable = 'YES'
        elif self.output_full:
            workable = f'{workable} (OUTPUT FULL)'
        else:
            workable = f'{workable} (MISSING INPUTS)'
        return workable

    def progress_repr(self):
        return f'{self.progress}%'

    def add_to_input_inventory(self, kind, amount):
        if hasattr(self, 'input_inventory'):
            self.input_inventory.add(kind, amount)
            return True
        else:
            logger.error(f'attempt was made to add to non-existent input inventory: {kind}, {amount}, target: {self.label}')
            return False

    def set_reprs(self):
        self.reprs = {
            'output': self.inventory_repr,
            'label': self.label_repr,
            'position': self.position_repr,
        }
        if self.workable:
            self.reprs['input'] = self.input_inventory_repr
            self.reprs['workable'] = self.workable_repr
            self.reprs['progress'] = self.progress_repr
