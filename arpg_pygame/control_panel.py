import logging
import pygame

from arpg_pygame.button import Button
from arpg_pygame import colors
from arpg_pygame.dynamic_text_group import DynamicTextGroup
from arpg_pygame.panel_card import PanelCard
from arpg_pygame.settings import config

logger = logging.getLogger(__name__)


class ControlPanel(pygame.sprite.Sprite):
    def __init__(self, game_obj):
        self.DEFAULT_HEIGHT = config.QUARTER_HEIGHT
        self.MIN_HEIGHT = config.unit / 2
        self.size = (config.SCREEN_WIDTH, self.DEFAULT_HEIGHT)
        self.position = (0, config.control_panel_vertical_pos)
        self.DEFAULT_POSITION = self.position
        self.MIN_POSITION = (0, config.SCREEN_HEIGHT - config.unit / 8)
        self.DEFAULT_TOGGLE_TEXT = 'v'
        self.toggle_text = self.DEFAULT_TOGGLE_TEXT
        self.draw_panel()
        self.set_up_content(game_obj)

    def set_up_content(self, game_obj):
        self.message_feed = DynamicTextGroup(
            position=config.control_panel_message_feed_pos,
            parent_sprite=self,
            group_type='list',
            label='messages',
            offset_mult=(0.025, 1.0),
            reversed_order=True,
            entry_limit=20,
        )
        game_obj.ui_containers.add(self.message_feed)

        self.structure_selector = DynamicTextGroup(
            position=config.control_panel_content_pos,
            parent_sprite=self,
            label='structures',
            offset_mult=(0.1, 1.0),
            show=False,
            display_type='card',
        )
        game_obj.ui_containers.add(self.structure_selector)
        game_obj.context_menus.add(self.structure_selector)
        self.structure_selector.set_all_text(config.buildable_structures)

        self.inventory_display = DynamicTextGroup(
            position=config.control_panel_content_pos,
            parent_sprite=self,
            offset_mult=(0.1, 1.0),
            label='inventory',
            display_type='card',
        )
        game_obj.ui_containers.add(self.inventory_display)

        self.colonist_display = DynamicTextGroup(
            position=config.control_panel_content_pos,
            parent_sprite=self,
            offset_mult=(0.1, 1.0),
            label='colonist',
            content_updater=game_obj.update_colonist_display_contents,
            show=False,
            display_type='card',
        )
        game_obj.ui_containers.add(self.colonist_display)

    def draw_panel(self):
        self.surf, self.rect = self.draw_surf(self.size, self.position)
        self.buttons = self.define_buttons()
        # positions relative to control panel surface origin
        pygame.draw.line(self.surf, colors.OFF_WHITE, (0, 0), (self.rect.width, 0), width=5)
        pygame.draw.line(
            self.surf, colors.OFF_WHITE,
            (config.control_panel_content_pos),
            (config.control_panel_content_pos[0], self.rect.height),
            width=5
        )
        pygame.draw.line(
            self.surf, colors.OFF_WHITE,
            config.control_panel_message_feed_pos,
            (config.control_panel_message_feed_pos[0], self.rect.height),
            width=5
        )
        pygame.draw.line(
            self.surf, colors.OFF_WHITE,
            config.control_panel_info_popup_pos,
            (config.control_panel_info_popup_pos[0], self.rect.height),
            width=5
        )

    def draw_surf(self, size, position):
        surf = pygame.Surface(size, pygame.SRCALPHA)
        rect = surf.get_rect(topleft=(position))
        surf.fill(colors.GRAY)
        surf.set_alpha(80)
        return surf, rect

    def define_buttons(self):
        buttons = {
            'toggle_control_panel': Button(
                self.toggle_text,
                x=config.SCREEN_WIDTH - config.unit,
                y=self.position[1] - config.unit * 0.25,
                button_action=self.toggle_console,
            ),
            'toggle_structures': Button(
                'Structures',
                x=self.position[0] + config.unit / 4,
                y=self.position[1] + config.unit / 4 + config.unit / 2 * 0,
                button_action=self.toggle_structures,
            ),
            # need a better name division here
            'toggle_stores': Button(
                'Stores',
                x=self.position[0] + config.unit / 4,
                y=self.position[1] + config.unit / 4 + config.unit / 2 * 1,
                button_action=self.toggle_stores,
            ),
            'toggle_colonists': Button(
                'Colonists',
                x=self.position[0] + config.unit / 4,
                y=self.position[1] + config.unit / 4 + config.unit / 2 * 2,
                button_action=self.toggle_colonists,
            ),
        }
        return buttons

    def draw(self, screen):
        screen.blit(self.surf, self.rect)
        for key, value in self.buttons.items():
            value.draw(screen)

    def toggle_console(self):
        if self.size[1] == self.DEFAULT_HEIGHT:
            self.size = (self.size[0], self.MIN_HEIGHT)
            self.position = self.MIN_POSITION
        else:
            self.size = (self.size[0], self.DEFAULT_HEIGHT)
            self.position = self.DEFAULT_POSITION
        self.toggle_text = '^' if self.toggle_text == self.DEFAULT_TOGGLE_TEXT else self.DEFAULT_TOGGLE_TEXT
        self.draw_panel()
        self.update_contents()

    def update_contents(self):
        self.colonist_display.update(force=True)
        self.inventory_display.update(force=True)
        self.message_feed.update(force=True)
        self.structure_selector.update(force=True)

    def hide_all_control_panel_views(self):
        self.colonist_display.show = False
        self.inventory_display.show = False
        self.structure_selector.show = False

    def toggle_structures(self):
        self.hide_all_control_panel_views()
        self.structure_selector.show = True
        logger.info('structure toggle')

    def toggle_stores(self):
        self.hide_all_control_panel_views()
        self.inventory_display.show = True
        logger.info('stores toggle')

    def toggle_colonists(self):
        self.hide_all_control_panel_views()
        self.colonist_display.show = True
        logger.info('colonists toggle')
