import math
import pygame

from arpg_pygame.settings import config
from arpg_pygame import colors

class Progress(pygame.sprite.Sprite):
    def __init__(self, parent_sprite, show=True):
        self.unit = config.grid_square_size / 2
        self.progress_type = parent_sprite.sprite_type
        self.parent_sprite = parent_sprite
        self.show = show
        self.progress = 0
        self.surf, self.rect = self.generate_sprite()
    def generate_sprite(self):
        surf = pygame.Surface((self.unit, self.unit), pygame.SRCALPHA)
        #surf.fill(colors.GRAY)
        radius = self.unit / 2
        # this rect position is relative to the surf on this object
        arc_rect = (0, 0, self.unit / 2, self.unit / 2)
        # sick circle math to draw starting at top, moving clockwise
        start_radians = math.pi / 2
        progress_radians = (-3.6 * self.progress + 90) * math.pi / 180
        pygame.draw.arc(surf, colors.ORANGE, arc_rect, progress_radians, start_radians, width=int(radius))
        if self.progress_type == 'structure':
            rect = surf.get_rect(topleft=self.parent_sprite.rect.topleft)
        else:
            rect = surf.get_rect(center=self.parent_sprite.rect.topleft)
        return surf, rect
    def update(self, progress):
        self.progress = progress
        self.surf, self.rect = self.generate_sprite()
