import pygame

from arpg_pygame import colors
from arpg_pygame.settings import config

class Text(pygame.sprite.Sprite):
    def __init__(self, font, color_fg, color_bg, x, y, prefix=None, label=None, parent_sprite=None, contents=None):
        pygame.sprite.Sprite.__init__(self)
        self.font = font
        self.color_fg = color_fg
        self.color_bg = color_bg
        self.x = x
        self.y = y
        self.prefix = prefix
        self.label = label
        self.parent_sprite = parent_sprite
        if contents:
            self.update(contents)
        else:
            self.update('')
        self.show = True

    def update(self, text):
        contents = text
        if self.prefix:
            contents = f'{self.prefix}{text}'
        contents_size = self.font.size(contents)
        # rather not have to recreate the Surface every update but couldn't get the size to change
        self.surf = pygame.Surface(contents_size)
        self.rect = self.surf.get_rect(topleft=(self.x, self.y))

        self.textSurf = self.font.render(contents, True, self.color_fg, self.color_bg)

        self.surf.blit(self.textSurf, (0, 0))

    def set_pos(self, pos):
        self.rect.center = pos


def build_text_object(text, font, color=colors.WHITE, bg_color=colors.BLACK):
    text_surface = font.render(text, True, color, bg_color)
    return text_surface, text_surface.get_rect()
