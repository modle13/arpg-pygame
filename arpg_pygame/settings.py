import math
import pygame

from arpg_pygame import colors
from arpg_pygame.menu_config import get_menu_defs
from arpg_pygame.data.structure import definitions

pygame.init()

class Config(object):
    def __init__(self, dictionary):
        self.__dict__.update(dictionary)

config = Config({})
# this will need to be more specific if other definitions get added
config.definitions = definitions
config.debug = False

#################################
##### SIZES AND DIMENSIONS  #####
#################################

# Define constants for the screen width and height
config.SCREEN_WIDTH = 1200
config.SCREEN_HEIGHT = 900
#config.SCREEN_WIDTH = 800
#config.SCREEN_HEIGHT = 600

# useful precalculations
config.THREE_QUARTER_WIDTH  = config.SCREEN_WIDTH  / 4 * 3
config.THREE_QUARTER_HEIGHT = config.SCREEN_HEIGHT / 4 * 3
config.HALF_WIDTH           = config.SCREEN_WIDTH  / 2
config.HALF_HEIGHT          = config.SCREEN_HEIGHT / 2
config.THIRD_WIDTH          = config.SCREEN_WIDTH  / 3
config.THIRD_HEIGHT         = config.SCREEN_HEIGHT / 3
config.QUARTER_WIDTH        = config.SCREEN_WIDTH  / 4
config.QUARTER_HEIGHT       = config.SCREEN_HEIGHT / 4
config.SCREEN_CENTER        = (config.HALF_WIDTH, config.HALF_HEIGHT)
#screen_divisor = 35
config.screen_divisor = 20
unit = int(config.SCREEN_WIDTH / config.screen_divisor)
config.unit = unit
config.grid_square_size = unit
config.tile_size = unit
config.world_size = 1.5
#config.world_size = 0.98
config.max_width  = config.SCREEN_WIDTH  * config.world_size
config.max_height = config.SCREEN_HEIGHT * config.world_size
# columns are related to x coords / width; offset by 1 to avoid black edges on right and bottom
config.num_grid_columns = int(config.max_width / unit) + 1
#config.num_grid_columns = int(config.max_width / unit) + 2
# rows are related to y coords / height
config.num_grid_rows = int(config.max_height / unit) + 1
#config.num_grid_rows = int(config.max_height / unit) + 2
config.grid_snap_offset = 5

config.control_panel_height = config.SCREEN_HEIGHT - config.THREE_QUARTER_HEIGHT
config.control_panel_vertical_pos = config.THREE_QUARTER_HEIGHT
config.control_panel_content_pos = (config.unit * 2.5, 0)
config.control_panel_message_feed_pos = (config.SCREEN_WIDTH - config.unit * 5, 0)
config.control_panel_info_popup_pos = (config.SCREEN_WIDTH - config.unit * 8, 0)
config.control_panel_cards_per_row = 6

config.max_screen_width  = config.max_width
config.max_screen_height = config.max_height + config.control_panel_height

config.movement_speed = 4

#################################
##### COLONIST SETTINGS     #####
#################################

# config.max_colonists = 15000
config.max_colonists = 15
# config.max_colonists = 3
config.colonist_def = {
    'default_role': 'porter',
    'default_action_archetype': 'porter',
    'hunger_threshold': 400,
    'inventory_size': 2,
    'speed': config.movement_speed,
    'start_food': 400,
    #'surf_size': (unit // 2, unit // 2),
    'surf_size': (unit // 4, unit // 4),
    'tick_time': 200,
}

#################################
##### RESOURCE SETTINGS     #####
#################################

config.max_creatures = 5
config.max_trees = 25
config.max_shrooms = 10
config.max_wheat = 10

config.growth_sizes = [
    int(unit / 5),
    int(unit / 5) * 2,
    int(unit / 5) * 3,
    int(unit / 5) * 4,
    unit,
]

config.type_sizes = {
    'creature': math.floor(len(config.growth_sizes) / 2),
    'shroom'  : math.floor(len(config.growth_sizes) / 2),
    'wheat'   : math.floor(len(config.growth_sizes) / 2),
    'dummy'   : len(config.growth_sizes)                ,
    'tree'    : len(config.growth_sizes)                ,
}

# seconds
config.growth_rate = {
    'min': 1,
    'max': 3,
}

config.resource_product_types = {
    'creature': {'meat'     : 2, 'hide'      : 1, 'bone' : 3, 'fat'    : 2,},
    'shroom':   {'shroombit': 3, 'compost'   : 1                          ,},
    'tree':     {'log'      : 3, 'compost'   : 2                          ,},
    'wheat':    {'straw'    : 1, 'wheat_germ': 2, 'grain': 2, 'compost': 2,},
}

config.resource_events = {
    'ADDCREATURE': {
        'max': config.max_creatures,
        'name': 'creature',
        'role': 'hunter',
    },
    'ADDTREE': {
        'max': config.max_trees,
        'name': 'tree',
        'role': 'woodcutter',
    },
    'ADDSHROOM': {
        'max': config.max_shrooms,
        'name': 'shroom',
        'role': 'forager',
    },
    'ADDWHEAT': {
        'max': config.max_wheat,
        'name': 'wheat',
        'role': 'thresher',
    },
}

config.food_values = {
    'soup': 600,
    'shroombit': 200,
}

#################################
##### FONT SETTINGS         #####
#################################

config.font = 'freesansbold.ttf'
config.MENU_FONT      = pygame.font.Font(config.font, int(110 / 1080 * config.SCREEN_HEIGHT))
config.LARGE_FONT     = pygame.font.Font(config.font, int(40  / 1080 * config.SCREEN_HEIGHT))
config.MEDIUM_FONT    = pygame.font.Font(config.font, int(35  / 1080 * config.SCREEN_HEIGHT))
config.SMALL_FONT     = pygame.font.Font(config.font, int(25  / 1080 * config.SCREEN_HEIGHT))
config.SMALLER_FONT   = pygame.font.Font(config.font, int(20  / 1080 * config.SCREEN_HEIGHT))
config.TINY_FONT      = pygame.font.Font(config.font, int(15  / 1080 * config.SCREEN_HEIGHT))
config.TINIER_FONT    = pygame.font.Font(config.font, int(10  / 1080 * config.SCREEN_HEIGHT))


#################################
##### STRUCTURE DEFINITIONS #####
#################################

# assume buildable if buildable flag not present
config.buildable_structures = list(
    dict(filter(
        lambda elem: True if 'buildable' not in elem[1] else elem[1]['buildable'],
        definitions.structures.items()
    )).keys()
)

config.starting_structures = [
    {
        'kind': 'town_center',
        # floor to snap it to the grid (should make that a common function)
        'position': (
            math.floor(config.THIRD_WIDTH  / unit) * unit - config.grid_snap_offset,
            math.floor(config.THIRD_HEIGHT / unit) * unit - config.grid_snap_offset,
        ),
    },
]

config.storage_structures = list(
    dict(
        filter(
            lambda x: x[1].get('target_type') == 'storage', definitions.structures.items()
        )
    ).keys()
)

config.storage_items = definitions.storage_items

config.initial_inventory = {
    'town_center': {
        'fertilizer': 3,
        'log': 3,
        'shroombit': 30,
        'compost': 3,
        'grain': 3,
        'wheat_germ': 3,
        'wheat_seed': 3,
        'soup': 30,
        'straw': 3,
    }
}


#################################
##### MENU AND UI SETTINGS  #####
#################################

config.menu_defs = get_menu_defs(config)
config.sprite_context_defs = {
    'colonist': {
        'fields': ['role', 'position', 'target', 'status', 'food', 'inventory',],
        'font': config.SMALL_FONT,
    },
    'resource': {
        'fields': ['label', 'position', 'progress', 'claimed',],
        'font': config.SMALL_FONT,
    },
    'product': {
        'fields': ['label', 'position', 'claimed',],
        'font': config.SMALL_FONT,
    },
    'structure': {
        'fields': ['label', 'position', 'workable', 'progress', 'input', 'output',],
        'font': config.SMALL_FONT,
    },
}

