class Inventory(object):
    def __init__(self, limit_type='individual', purpose='output', limit=10, kinds=[]):
        self.purpose = purpose
        if limit_type == 'individual' and purpose == 'output':
            assert(kinds), 'list of inventory kinds required if individual limits'
        self.limit_type = limit_type
        self.limit = limit
        self.kinds = kinds
        self.contents = {kind: 0 for kind in kinds}
        self.show = True
    def add(self, kind, amount) -> int:
        remaining_capacity = self.has_room(kind)
        to_add = remaining_capacity if remaining_capacity < amount else amount
        try:
            self.contents[kind] += to_add
        except KeyError:
            self.contents[kind] = to_add
        return to_add
    def remove(self, kind, amount) -> int:
        if not kind:
            return 0
        current = self.get_available_count(kind) or 0
        to_remove = 0
        if current:
            to_remove = current if current < amount else amount
            new_amount = current - to_remove
            self.set(kind, new_amount)
        return to_remove
    def set(self, kind, amount):
        self.contents[kind] = amount
    def is_full(self, kind=None) -> int:
        return self.has_room(kind=kind) == 0
    def is_empty(self, kind=None) -> int:
        return self.has_room(kind=kind) == self.limit
    def get_capacity(self, kind=None) -> int:
        return self.has_room(kind=kind)
    def has_room(self, kind=None) -> int:
        if self.limit_type == 'unlimited':
            return float('inf')
        if kind and kind not in self.contents:
            return 0
        count = self.get_available_count(kind=kind)
        remaining_capacity = 0 if self.limit_type == 'individual' else self.limit
        if count or self.limit_type == 'individual':
            remaining_capacity = self.limit - count
        return remaining_capacity
    def get_available_count(self, kind=None) -> int:
        if self.limit_type == 'total' or not kind:
            count = sum(self.contents.values())
        else:
            count = self.contents.get(kind)
        return count or 0
    def contains(self, kind) -> bool:
        return bool(self.contents.get(kind))
    def get_kinds(self) -> list:
        return list(self.contents.keys())
    def has_amount(self, kind, amount) -> bool:
        found = self.contents.get(kind)
        return found != None and bool(found >= amount)
    def reset(self):
        self.contents = {}
    def toggle(self):
        self.show = not self.show
    def get_contents_repr(self) -> list:
        items = list(self.contents.items())
        contents_repr = [f'{x[0]}: {x[1]}' for x in items]
        return contents_repr
    def __repr__(self):
        text = []
        for key, value in self.contents.items():
            if self.limit_type == 'unlimited':
                text.append(f'{key} {value}')
            else:
                text.append(f'{key} {value}/{self.limit}')
        return ', '.join(text)
