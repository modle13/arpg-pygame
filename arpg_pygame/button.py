import pygame

from arpg_pygame import colors
from arpg_pygame.settings import config


class Button:
    def __init__(self, text, x=0, y=0, width=0, height=0, color=colors.GREEN, button_action=None):
        self.pressed = False
        self.text = text
        self.position = (x, y)
        self.button_font = pygame.font.Font('freesansbold.ttf', 20)

        button_dimensions = self.get_button_dimensions(text, width, height)
        self.button_rect = pygame.Rect(self.position, button_dimensions)
        # button color constant for later reset
        self.BUTTON_COLOR = color
        self.button_color = color

        self.text_surf = self.button_font.render(self.text, True, colors.BLACK)
        self.text_rect = self.text_surf.get_rect(center=self.button_rect.center)
        if button_action:
            self.button_action = button_action

    def get_button_dimensions(self, text: str, width: int, height: int) -> tuple:
        """Calculate width and height of font based on size of text string if no dimensions provided."""
        # use font object to get base width/height of rendered text
        font_width, font_height = self.button_font.size(text)
        # adjust size if needed for padding
        if width:
            font_width = config.unit
        else:
            font_width += 0.25 * config.unit
        if height:
            font_height = height
        return (font_width, font_height)

    def draw(self, screen):
        pygame.draw.rect(screen, self.button_color, self.button_rect, border_radius=5)

        screen.blit(self.text_surf, self.text_rect)
        self.check_click()

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()

        if self.button_rect.collidepoint(mouse_pos):
            self.button_color = colors.WHITE
            if pygame.mouse.get_pressed()[0]:
                self.pressed = True
                self.button_color = colors.GREEN
            else:
                if self.pressed:
                    self.button_action()
                    self.pressed = False
        else:
            self.pressed = False
            self.button_color = self.BUTTON_COLOR

    def button_action(self):
        print('button pressed')
