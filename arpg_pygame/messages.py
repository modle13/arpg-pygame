import pygame

from arpg_pygame import colors
from arpg_pygame.settings import config

class Message(pygame.sprite.Sprite):
    def __init__(self, contents, duration):
        pygame.sprite.Sprite.__init__(self)
        # create a font object.
        # 1st parameter is the font file, which is present in pygame.
        # 2nd parameter is size of the font
        self.font = pygame.font.Font('freesansbold.ttf', 32)

        # create a text surface object, on which text is drawn
        self.text = self.font.render(contents, True, colors.GREEN, colors.BLUE)
        self.duration = duration
        self.elapsed = 0

        # create a rectangular object for the text surface object
        self.textRect = self.text.get_rect()

        # set the center of the rectangular object.
        self.textRect.center = (config.SCREEN_WIDTH - 20, 20)

    # Remove the text after time elapses
    def update(self):
        self.elapsed += 1
        if self.elapsed > self.duration:
            self.kill()
