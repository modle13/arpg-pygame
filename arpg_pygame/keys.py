# Import pygame.locals for easier access to key coordinates
# available keys: https://www.pygame.org/docs/ref/key.html#module-pygame.key
from pygame.locals import (
    RLEACCEL,
    # movement
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_w,
    K_a,
    K_s,
    K_d,
    # toggles
    K_b,
    K_c,
    K_i,
    K_l,
    K_F2,
    K_F3,
    K_F4,
    K_F5,
    # others
    K_LALT,
    K_DELETE,
    K_ESCAPE,
    K_LSHIFT,
    K_PRINT,
    K_RETURN,
    KEYDOWN,
    KEYUP,
    MOUSEBUTTONDOWN,
    MOUSEBUTTONUP,
    QUIT,
)

"""
# show all available locals
from pygame import locals
import json
print(json.dumps(dir(locals), indent=4, sort_keys=True))
"""
