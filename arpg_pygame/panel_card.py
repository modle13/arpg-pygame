import pygame
from arpg_pygame.settings import config
from arpg_pygame import colors


class PanelCard(pygame.sprite.Sprite):
    def __init__(self, x=0, y=0, content='placeholder', amount=0, card_count=0):
        pygame.sprite.Sprite.__init__(self)

        BORDER = 5
        WIDTH = 90
        HEIGHT = 50
        HALF_WIDTH = WIDTH / 2
        HALF_HEIGHT = HEIGHT / 2

        self.x_offset = card_count % config.control_panel_cards_per_row
        # rounds down
        self.y_offset = int(card_count / config.control_panel_cards_per_row)

        self.x = x + self.x_offset * (WIDTH + BORDER)
        self.y = y + self.y_offset * (HEIGHT + BORDER)

        self.content = content or 'default'
        # for structure selector use
        self.label = self.content
        self.amount = amount

        self.font = config.TINY_FONT
        self.show = True

        content_size = self.font.size(content)

        # color in rect with a border
        self.surf = pygame.Surface((WIDTH, HEIGHT))
        self.rect = self.surf.get_rect(topleft=(self.x, self.y))
        self.surf.fill(colors.GRAY)

        # draw outline; inflate offsets too annoying to deal with for later positioning
        # unless we find an easier way to get an outline of a rect
        pygame.draw.line(self.surf, colors.OFF_WHITE, (0, 0), (WIDTH, 0), width=3)
        pygame.draw.line(self.surf, colors.OFF_WHITE, (WIDTH, 0), (WIDTH, HEIGHT), width=3)
        pygame.draw.line(self.surf, colors.OFF_WHITE, (WIDTH, HEIGHT), (0, HEIGHT), width=3)
        pygame.draw.line(self.surf, colors.OFF_WHITE, (0, HEIGHT), (0, 0), width=3)

        self.labelSurf = self.font.render(content, True, colors.OFF_WHITE)
        self.surf.blit(self.labelSurf, (BORDER, BORDER))
        self.countSurf = self.font.render(str(self.amount), True, colors.OFF_WHITE)
        self.surf.blit(self.countSurf, (BORDER, HALF_HEIGHT + BORDER))

        # bottom section horizontal
        pygame.draw.line(self.surf, colors.OFF_WHITE, (0, HALF_HEIGHT), (WIDTH, HALF_HEIGHT), width=3)
        pygame.draw.line(self.surf, colors.OFF_WHITE, (HALF_WIDTH, HALF_HEIGHT), (HALF_WIDTH, HEIGHT), width=3)

    def update(self, text):
        return
        self.contents = text
        if self.prefix:
            contents = f'{self.prefix}{text}'
