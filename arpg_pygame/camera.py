# http://stackoverflow.com/questions/14354171/add-scrolling-to-a-platformer-in-pygame/14357169#14357169

import pygame

from arpg_pygame.settings import config

class Camera(object):
    def __init__(self, width, height):
        self.state = pygame.Rect(0, 0, width, height)

    def process_camera(self, camera, target_rect):
        l, t, _, _ = target_rect # l = left,  t = top
        _, _, w, h = camera      # w = width, h = height

        x_offset = -l
        y_offset = -t
        return pygame.Rect(x_offset, y_offset, w, h)

    def apply(self, target):
        if type(target) == pygame.Rect:
            return target.move(self.state.bottomright)
        else:
            return target.rect.move(self.state.bottomright)

    def apply_pos(self, target):
        return target.rect.move(self.state.bottomright)

    def update(self, target):
        self.state = self.process_camera(self.state, target.rect)
