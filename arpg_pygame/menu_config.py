controls_menu_details = [
    'F2 : take screenshot',
    'F3 : debug',
    'F4 : stop colonist movement',
    'F5 : toggle grid overlay',
    'B : toggle structure list',
    'I : toggle inventory',
    'L : toggle message feed',
    'C : toggle colonist list',
    'ESC : close all menus or show this menu',
    'LEFT SHIFT : hold during placement to place multiple',
    'LEFT ALT + ESC : exit the game',
    'RIGHT CLICK : toggle a structure',
    'DELETE + RIGHT CLICK : remove a structure',
]


def get_menu_defs(config):
    menu_defs = {
        'controls': get_controls_defs(config),
    }
    return menu_defs


def get_controls_defs(config):
    background_size = (int(2 * config.SCREEN_WIDTH / 3), config.SCREEN_HEIGHT)
    controls_defs = {
        'background_size': background_size,
    }
    starting_height = int(background_size[1] / 4)
    horizontal_position = int(background_size[0] / 2)
    entries = []
    for index, value in enumerate(controls_menu_details):
        text_details = {
            'text': value,
            'font': config.SMALL_FONT,
            'position': (horizontal_position, starting_height + 20 * index),
        }
        entries.append(text_details)
    controls_defs['entries'] = entries
    return controls_defs
