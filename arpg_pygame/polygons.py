import math
import pygame

from arpg_pygame import colors
from arpg_pygame.settings import config

def get_creature_polygon():
    return [
        #top left, moving clockwise around contour
        # head
        (0    , 0    ),
        (2 / 6, 0    ),
        # body
        (2 / 6, 2 / 6),
        (1    , 2 / 6),
        # back leg
        (1    , 1    ),
        (5 / 6, 1    ),
        # belly
        (5 / 6, 4 / 6),
        # front leg
        (3 / 6, 4 / 6),
        (3 / 6, 1    ),
        (2 / 6, 1    ),
        # chest
        (2 / 6, 2 / 6),
        # head
        (0    , 2 / 6),
    ]

def get_tree_polygon():
    return [
        (1 / 4, 1    ),
        (1 / 2, 1 / 4),
        (3 / 4, 1    )
    ]

def get_wheat_polygon():
    return [
        (0    , 1    ), #start, at left
        (1 / 6, 1 / 3), # first peak
        (2 / 6, 1    ), # first valley
        (3 / 6, 1 / 3), # second peak
        (4 / 6, 1    ), # second valley
        (5 / 6, 1 / 3), # third peak
        (1    , 1    ), # finish
    ]

resources = {
    'creature': {'generator': get_creature_polygon, 'color': colors.TAN         ,},
    'tree'    : {'generator': get_tree_polygon ,    'color': colors.LIGHT_GREEN ,},
    'wheat'   : {'generator': get_wheat_polygon,    'color': colors.LIGHT_YELLOW,},
}


def get_resource_polygon(name, size):
    polygon_points = resources.get(name).get('generator')()
    points = list(map(lambda x: (x[0] * size, x[1] * size), polygon_points))
    return points

def draw_shape(surf, obj):
    """Entry point."""
    side = config.growth_sizes[obj.size]
    if obj.resource_type in ['tree', 'wheat', 'creature']:
        # polygon
        draw_polygon(surf, obj.resource_type, side)
    elif obj.resource_type == 'shroom':
        # circle
        position = (side / 2, side / 2)
        radius = side / 2
        pygame.draw.circle(surf, colors.LIGHT_BROWN, position, radius)
    elif obj.resource_type == 'disabled':
        position = (obj.unit / 2, obj.unit / 2)
        radius_outer = obj.unit / 2
        radius_inner = 3 * obj.unit / 8
        pygame.draw.circle(surf, colors.RED, position, radius_outer)
        pygame.draw.circle(surf, colors.BLACK, position, radius_inner)
        line_start = get_pos_on_circle(position, radius_inner, 315)
        line_end = get_pos_on_circle(position, radius_inner, 135)
        pygame.draw.line(surf, colors.RED, line_start, line_end, width=5)
    else:
        # square
        surf = pygame.Surface((side, side))
        surf.fill(colors.GRAY)


def get_pos_on_circle(position, radius, angle):
    angle_rad = math.radians(angle)
    pos_x = position[0] + radius * math.sin(angle_rad)
    pos_y = position[1] - radius * math.cos(angle_rad)
    return (pos_x, pos_y)


def draw_polygon(surf, kind, side):
    polygon = get_resource_polygon(kind, side)
    pygame.draw.polygon(surf, resources.get(kind).get('color'), polygon)
