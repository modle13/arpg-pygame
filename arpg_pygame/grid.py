import math
import pygame
import random

from arpg_pygame import colors
from arpg_pygame import keys
from arpg_pygame.product import Product
from arpg_pygame.progress import Progress
from arpg_pygame.settings import config


# much of this logic will get lifted to a 'workable' object, building, etc.; see issue 9
# these should not have workable properties, but should be static entities
# workable objects would get drawn on top? would replace?
# not every grid square will be a workable object, so danger of god object
# this should be bare-bones with position/color properties only
class GridSquare(pygame.sprite.Sprite):
    def __init__(self, x, y, color):
        super(GridSquare, self).__init__()
        self.border_width = 1
        self.start = (x, y)
        self.am_clicked = False
        self.surf = pygame.Surface((config.grid_square_size, config.grid_square_size))
        self.rect = self.surf.get_rect(topleft=self.start)
        self.border_color = color
        self.set_default_border()
        self.active = False
        self.claimed = False
        self.target_type = 'grid'
        self.sprite_type = 'grid'
        self.label = 'grid'
        self.progress = 0
        self.progress_display = Progress(self)
        self.complete_threshold = 100
        self.show = True
        self.target_size = 1

    def activate(self):
        self.surf.fill(colors.GRAY)
        self.active = True
        self.claimed = False

    def deactivate(self):
        self.set_default_border()
        self.active = False
        self.claimed = False

    def set_default_border(self):
        # to make border, first fill whole rect with single color
        self.surf.fill(self.border_color)
        # then fill all but 2 pixels on edge with black
        self.surf.fill(colors.BLACK, self.surf.get_rect().inflate(-self.border_width * 2, -self.border_width * 2))

    def work(self):
        self.progress += 3
        completed = self.progress >= self.complete_threshold
        if completed:
            self.progress -= self.complete_threshold
        self.progress_display.update(self.progress)
        return completed

    def process_work_complete(self):
        # attrs can be custom
        # 3 params needed: object to create, where to put it, how many to create
        # quantity would vary with type of resource/building being worked
        position = (self.rect.center[0], self.rect.center[1])
        # create the event with ADDPRODUCT ID
        make_thing = pygame.event.Event(config.game.events.ADDPRODUCT, product=Product, product_type='dummy', position=position, quantity=3)
        # post event to event queue
        pygame.event.post(make_thing)

    def on_click(self, mouse_pos):
        if config.camera.apply(self).collidepoint(mouse_pos):
            if self.active:
                self.deactivate()
            else:
                self.activate()


def generate_squares():
    sprites = []
    block_size = config.grid_square_size
    for x in range(config.num_grid_columns):
        for y in range(config.num_grid_rows):
            # TODO: uncertain why need to subtract 5 here, but otherwise, need to add 5 to snap
            # and alignment of grid squares is off
            # could possibly be caused by border thickness
            x_pos = x * block_size - 5
            y_pos = y * block_size - 5
            color = colors.GRAY
            square = GridSquare(x_pos, y_pos, color)
            sprites.append(square)
    return sprites


def snap_to_grid(position):
    unit = config.grid_square_size
    snapped_position = (math.floor(position[0] // unit) * unit, math.floor(position[1] // unit) * unit)
    return snapped_position


def get_random_grid_square(squares=None):
    if not squares:
        squares = get_unpopulated_tiles().sprites()
    try:
        random_sprite = random.choice(squares)
    except IndexError:
        random_sprite = None
    return random_sprite


def get_occupied_tiles(sprite_group):
    occupied_tiles = pygame.sprite.groupcollide(sprite_group, config.game.blocking_sprites, False, False)
    return occupied_tiles


def get_unpopulated_tiles():
    unoccupied_squares = config.game.grid_sprites.copy()
    occupied_squares = get_occupied_tiles(unoccupied_squares)
    # this is a dict
    unoccupied_squares.remove(occupied_squares)
    return unoccupied_squares


def get_collide_grid_square(position):
    unoccupied_squares = get_unpopulated_tiles()
    for grid_square in unoccupied_squares:
        if config.camera.apply(grid_square).collidepoint(position):
            return grid_square
    return None
