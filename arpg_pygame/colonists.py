import json
import logging
import pygame
import random

from arpg_pygame import colors
from arpg_pygame.context_pane import ContextPane
from arpg_pygame import grid
from arpg_pygame.inventory import Inventory
from arpg_pygame import movement
from arpg_pygame.product import Product
from arpg_pygame.settings import config
from arpg_pygame.target import Target
from arpg_pygame.text import Text

"""
known issues:
    wheat farmer is getting fertilizer after seeds are exhausted
        after which inventory seed count keeps going up (?)
        should be taking fertilizer to wheat farmer structure to make more wheat seed?
        does this happen to tree planters?
            no
        this seems to be 2 problems:
            wheat farmer is taking wheat seeds that never got produced/don't exist
            wheat farmer is turning those wheat seeds into infinite seeds
            wheat farmer is not gathering all materials/dumping those materials to the wheat farmer building
                is this the inventory missing error?
    porters will not offload inventory if inventory not full
    porters will sometimes stay in one place even with a target
    woodcutter/forester may not be sustainable because forester will
        plant outside woodcutter range
"""

logger = logging.getLogger(__name__)

class State(object):
    def __init__(self):
        self.find_in_storage         = False
        self.find_product            = False
        self.find_resource           = False
        self.has_inventory           = False
        self.hungry                  = False
        self.initialized             = False
        self.planting                = False
        self.select_new_target       = False
        self.store_at_storage        = False
        self.store_at_workable       = False
        self.work_assignment_ready   = False
        self.has_workable_assignment = False
    def reset(self):
        self.__init__()
    def __repr__(self):
        return json.dumps(self.__dict__, indent=4, sort_keys=True)


# Define the colonist object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'colonist'
class Colonist(pygame.sprite.Sprite):
    def __init__(self):
        super(Colonist, self).__init__()
        # all colonists are the default role until structures are built
        self.default_color = (random.randrange(120, 220), random.randrange(120, 220), random.randrange(120, 220))
        self.state = State()
        self.set_defaults()
        self.set_reprs()
        self.label = 'colonist'
        self.status_message = ''
        self.food = config.colonist_def.get('start_food')
        self.tick_time = config.colonist_def.get('tick_time')
        self.last_tick = 0
        self.sprite_type = 'colonist'
        self.inventory = Inventory(limit_type='total', limit=config.colonist_def.get('inventory_size'))
        self.debug_repr_sprite = Text(config.TINY_FONT, colors.WHITE, colors.BLACK, 0, -5)
        # top-left of game world
        #self.start = (config.HALF_WIDTH, config.HALF_HEIGHT)
        self.start = (0, 0)
        self.surf = pygame.Surface(config.colonist_def.get('surf_size'))
        # move to config?
        self.colors = {
            'dummy': self.default_color,
            'grid': colors.LIGHT_BLUE,
            'resource': colors.LIGHT_BLUE,
            'product': colors.MAGENTA,
            'storage': colors.YELLOW,
        }
        self.surf.fill(colors.GREEN)
        self.rect = self.surf.get_rect(center=self.start)
        self.context_pane = ContextPane(self, self.sprite_type)
        self.status_message = "Just born"
        self.status_text_object = None
        self.speed = config.colonist_def.get('speed')
        self.show = True
        # using a list to take advantage of pop()
        self.path_line_segments = []
        self.state_functions = {
            'porter'    : self.manage_state_porter    ,
            'processor' : self.manage_state_processor ,
            'harvester' : self.manage_state_harvester ,
            'cultivator': self.manage_state_cultivator,
        }
        self.move = True
        self.action = 'idle'
        self.actions = {
            'search': {
                'find_assignment'        : self.get_current_assignment ,# search
                'find_fallen_product'    : self.get_role_target        ,# search
                'find_input_deliver'     : self.get_current_assignment ,# search
                'find_output_deliver'    : self.get_nearest_storage    ,# search
                'find_output_retrieve'   : self.get_storage_target     ,# search
                'find_plantable'         : self.get_random_cultivable  ,# search
                'find_resource'          : self.get_role_target        ,# search
            },
            'proximity': {
                'gather_item'            : self.retrieve_fallen_product,# target nearby
                'harvest'                : self.remove_resource        ,# target nearby
                'process'                : self.complete_processing    ,# target nearby
                'plant'                  : self.plant_item             ,# target nearby
                'reach_target'           : self.handle_work_complete   ,# target nearby
                'retrieve_from_storage'  : self.retrieve_from_storage  ,# target nearby
                'store_at_input'         : self.add_to_input_inventory ,# target nearby
                'store_at_storage'       : self.deposit_in_storage     ,# target nearby
            },
        }

    def update(self):
        self.update_conditions()

        if not self.action:
            self.manage_search_action()

        if not self.state.initialized:
            self.do_something_new()

        if self.check_target_nearby():
            if self.role == 'porter':
                logger.debug(f'{self.role} has target nearby? {self.check_target_nearby}')
            self.move = False
            self.handle_work()
        elif self.role == 'hunter':
            # hate to have to do hunter check here, but they're a weird case
            # because their target moves
            self.perform_hunter_target_check()

        if self.move:
            self.manage_movement()

        self.context_pane.update()
        self.update_status_text()
        self.update_repr()
        self.set_color()

        self.check_for_obstacles()

    def do_something_new(self):
        self.manage_state()
        self.manage_search_action()
        self.manage_target()

    def perform_hunter_target_check(self):
        # kickstart the stuck hunter
        if not self.move:
            self.move = True

    def manage_movement(self):
        # just move
        if config.game.colonists_active:
            movement.move(self)

    def handle_work(self):
        complete = self.target.work()
        if complete:
            logger.debug(f'{self.role} work complete on {self.target.label}')
            self.manage_proximity_action()
            target_func = self.actions.get('proximity').get(self.action)
            if target_func:
                target_func()
            else:
                logger.error(f'did not find target function for action: {self.action}')
            self.state.reset()


    #################################
    ##### STATE MANAGEMENT      #####
    #################################

    def manage_state(self):
        # top level state management here
        self.state.hungry = self.food < config.colonist_def.get('hunger_threshold')
        self.state.select_new_target = not self.target or (self.assignment and not self.assignment.active)
        # else get into the weeds by type
        if not self.state.hungry:
            archetype_state_function = self.state_functions.get(self.action_archetype)
            archetype_state_function()
        self.state.initialized = True

    def manage_state_porter(self):
        full_inventory = self.inventory.is_full()
        self.state.has_inventory = not self.inventory.is_empty()
        self.state.find_product = not full_inventory
        self.state.store_at_storage = not self.state.find_product
        #logger.info(f'porter state is: {self.state}')

    def manage_state_harvester(self):
        self.state.find_resource = self.assignment is not None

    def manage_state_processor(self):
        full_inventory = self.inventory.is_full()
        self.state.has_inventory = not self.inventory.is_empty()
        self.state.work_assignment_ready = self.assignment.ready
        self.state.store_at_workable = self.state.has_inventory
        self.state.find_in_storage = (
            not self.state.work_assignment_ready
            and not self.assignment.full
            and not full_inventory
        )
        # always true for processor
        self.state.has_workable_assignment = True

    def manage_state_cultivator(self):
        self.manage_state_processor()
        self.state.planting = self.has_plantable()
        self.state.find_in_storage = self.state.find_in_storage or self.plantable_available()

    def update_conditions(self):
        # time in milliseconds since start
        current = pygame.time.get_ticks()
        if current - self.last_tick > self.tick_time and self.food > 0:
            #self.food -= 1
            self.last_tick = current

    def set_color(self):
        if self.target and self.target.target_type:
            self.surf.fill(self.colors.get(self.target.target_type))

    def complete_processing(self):
        self.handle_work_complete()

    def retrieve_fallen_product(self):
        if not self.check_target_nearby():
            return
        # picking up product
        added = self.add_to_inventory(self.target.product_type, 1)
        if added:
            self.target.destroy()
        else:
            self.target.claimed = False
        self.handle_work_complete()

    def plant_item(self):
        # creating resource spawn event
        resource_id = self.assignment.cultivate_data.get('event_id')
        event_id = getattr(config.game.events, resource_id)
        make_thing = pygame.event.Event(event_id, position=self.target.rect.center)
        # post event to event queue
        pygame.event.post(make_thing)
        name = self.assignment.cultivate_data.get('name')
        self.inventory.remove(name, 1)
        config.game.message_feed.add(f'{self.role} planted a {name}')
        self.handle_work_complete()

    def remove_resource(self):
        config.game.message_feed.add(f'{self.role} wrecked a {self.target.label}')
        self.target.destroy()
        self.handle_work_complete()

    def handle_work_complete(self):
        #self.target = self.create_target()
        self.target = None
        self.trigger_state_reset()
        self.path_line_segments = []
        self.set_color()

    def deposit_in_storage(self):
        # adding to storage
        self.target.store(self.inventory.contents)
        # drop anything left
        #   next cycle will determine if it should be picked up again
        #   if there is available storage
        self.drop_all_inventory()
        self.handle_work_complete()

    def retrieve_from_storage(self):
        kind = self.determine_item_kind()
        amount = 0
        if kind in self.assignment.input_inventory.get_kinds():
            amount = self.assignment.input_inventory.get_capacity(kind)
        elif kind in self.assignment.output_inventory.get_kinds():
            amount = self.target.output_inventory.get_available_count(kind)
        capacity = self.inventory.get_capacity(kind)
        amount = amount if amount <= capacity else capacity
        retrieved = self.target.retrieve(kind, amount)
        self.add_to_inventory(kind, retrieved)
        if self.inventory.is_full() or not self.target.output_inventory.get_available_count(kind):
            self.trigger_state_reset()

    def add_to_input_inventory(self):
        for kind, amount in self.inventory.contents.items():
            if amount:
                added = self.target.add_to_input_inventory(kind, amount)
                if added:
                    self.inventory.remove(kind, amount)
                else:
                    # see issue #83 for details
                    # (is it right to put a link here?)
                    # https://gitlab.com/modle13/colony-sim-pygame/-/issues/83
                    # something went wrong, so just give up and try something else
                    # possible outcome is target does not have input inventory
                    # this seems to trigger about once every 5 minutes on average
                    #     with a single wheat_farmer
                    logger.info(f'triggering state reset due to failure to add to input inventory: {self.assignment.label}, {amount} of {kind} to {self.target.label}')
                    self.trigger_state_reset()

    def assign(self, assignment):
        if hasattr(self, 'target') and self.target:
            self.target.claimed = False
        self.assignment = assignment
        # self.target = self.create_target()
        if self.target and self.target.target_type == 'dummy':
            self.target.expired = True
        self.trigger_state_reset()
        self.drop_all_inventory()

    def unassign_role(self):
        self.set_defaults()

    def set_defaults(self):
        self.trigger_state_reset()
        self.role = config.colonist_def.get('default_role')
        self.action_archetype = config.colonist_def.get('default_action_archetype')
        self.action = ''
        if hasattr(self, 'target') and self.target:
            self.target.claimed = False
        self.target = self.create_target()
        self.assignment = None

    def trigger_state_reset(self):
        self.action = ''
        self.path_line_segments = []
        if hasattr(self, 'target') and self.target:
            self.target.claimed = False
            self.target = None
        self.state.reset()


    #################################
    ##### INVENTORY MANAGEMENT  #####
    #################################

    def determine_item_kind(self):
        kind = None
        found_storages = None
        if self.state.hungry:
            kind = self.get_food_type()
        elif self.action_archetype == 'cultivator':
            # find cultivating item first
            kind = self.assignment.cultivate_data.get('name')
            found_storages = self.get_nearest_storage_with(kind)
        if (not kind or not found_storages) and self.state.has_workable_assignment:
            # otherwise find input types
            kind = self.get_most_needed_input_type()
        return kind

    def get_food_type(self):
        food_types = config.food_values
        found = None
        for food in food_types:
            found_storages = self.get_nearest_storage_with(food)
            if not found_storages:
                continue
            found = food
            break
        return found

    def get_most_needed_input_type(self):
        input_types = self.assignment.input_types
        least = float('inf')
        target_item = None
        for item in input_types:
            has_room = self.assignment.input_inventory.has_room(item)
            count = self.assignment.input_inventory.get_available_count(item)
            if count < least and has_room:
                found_storages = self.get_nearest_storage_with(item)
                if not found_storages:
                    continue
                target_item = item
                least = count
        return target_item

    def plantable_available(self):
        kind = self.assignment.cultivate_data.get('name')
        filtered_storages = list(filter(lambda x: x.check_inventory_for(kind), config.game.storage_structures))
        if filtered_storages:
            return True
        return False

    def has_plantable(self):
        kind = self.assignment.cultivate_data.get('name')
        unit_amount = self.inventory.contains(kind)
        kind_available = unit_amount != None and unit_amount > 0
        return kind_available

    def add_to_inventory(self, kind, amount):
        added = self.inventory.add(kind, amount)
        return added

    def drop_all_inventory(self):
        for key, value in self.inventory.contents.items():
            if not value:
                continue
            make_thing = pygame.event.Event(config.game.events.ADDPRODUCT, product=Product, product_type=key, position=self.rect.center, quantity=value)
            # post event to event queue
            pygame.event.post(make_thing)
            if self.target:
                config.game.message_feed.add(f'{self.target.label} is full of {key}')
            config.game.message_feed.add(f'{self.role} had nowhere to put {key}')
        self.inventory.reset()


    #################################
    ##### TARGET MANAGEMENT     #####
    #################################

    def manage_target(self):
        if self.state.select_new_target:
            # relinquish claim on current target
            if self.target:
                self.target.claimed = False
            new_target = self.check_for_target()
            if new_target:
                new_target.claimed = True
                self.target = new_target
            else:
                self.target = self.default_target
            # attempted target find, so set action to move
            self.move = True
            if self.target:
                movement.calculate_path(self, self.target)

    def manage_search_action(self):
        # hunger is priority, colonists will do nothing else when hungry
        if self.state.hungry:
            self.action = 'find_output_retrieve'
            return

        # this action split/check is where it would be useful to have separate role objects

        if self.action_archetype == 'porter':
            if ((self.target and self.target.target_type == 'dummy') or
                not self.target) and self.state.find_product:
                self.action = 'find_fallen_product'
                self.backup_action = 'find_output_deliver'
            elif self.state.has_inventory:
                self.action = 'find_output_deliver'
            return

        if self.action_archetype == 'cultivator':
            if self.state.planting:
                self.action = 'find_plantable'
            elif self.state.store_at_workable:
                self.action = 'find_input_deliver'
            elif self.state.find_in_storage:
                self.action = 'find_output_retrieve'
            elif self.state.work_assignment_ready:
                self.action = 'find_assignment'
            return

        # cultivator is special; operates like a processor when no plantables
        if self.action_archetype == 'processor':
            if self.state.store_at_workable:
                self.action = 'find_input_deliver'
            elif self.state.find_in_storage:
                self.action = 'find_output_retrieve'
            elif self.state.work_assignment_ready:
                self.action = 'find_assignment'
            return

        if self.action_archetype == 'harvester':
            self.action = 'find_resource'

    def manage_proximity_action(self):
        if self.state.hungry:
            self.action = 'retrieve_from_storage'
            return

        # this action split/check is where it would be useful to have separate role objects

        if self.target.target_type == 'dummy':
            self.action = 'reach_target'
            return

        if self.action_archetype == 'porter':
            if self.target.target_type == 'product':
                self.action = 'gather_item'
            elif self.target.target_type == 'storage':
                self.action = 'store_at_storage'
            return

        if self.action_archetype == 'cultivator':
            if self.state.planting:
                self.action = 'plant'
            elif self.state.store_at_workable:
                self.action = 'store_at_input'
            elif self.state.work_assignment_ready:
                self.action = 'process'
            elif self.state.find_in_storage:
                self.action = 'retrieve_from_storage'

        if self.action_archetype == 'processor':
            if self.state.store_at_workable:
                self.action = 'store_at_input'
            elif self.state.work_assignment_ready:
                self.action = 'process'
            elif self.state.find_in_storage:
                self.action = 'retrieve_from_storage'

        if self.action_archetype == 'harvester':
            self.action = 'harvest'

    def check_for_target(self):
        """
        NOTE: try to avoid changing state here; use the manage_state function to set flags
            then reference those flags here to make decisions about targets
        
        Expected Behavior:
            find and return workable target if available
              otherwise
                dump any inventory in available storage
                or return a dummy target for idling
        """

        # reuse current target to avoid creating a new object every frame
        self.default_target = self.target

        target_func = self.actions.get('search').get(self.action)
        if not target_func:
            logger.info(f'no target found for action: {self.action}')
            return self.create_target()

        target = target_func()
        # reset action
        self.action = 'reach_target' if target and target.target_type == 'dummy' else ''

        if not target:
            target = self.create_target()

        return target

    def get_current_assignment(self):
        return self.assignment

    def create_target(self):
        target = Target(self.default_color)
        return target

    def get_storage_target(self):
        kind = self.determine_item_kind()
        storage_target = self.get_nearest_storage_with(kind)
        target = storage_target
        return target

    def get_role_target(self):
        targets = config.active_targets.get(self.role)
        filtered_targets = list(
            filter(
                lambda x: not x.claimed and self.in_structure_range(x), targets
            )
        )
        valid_target = movement.get_closest_target(self, filtered_targets)
        return valid_target

    def get_random_cultivable(self):
        if self.target and self.target.target_type == 'grid':
            return self.target
        unpopulated_squares = grid.get_unpopulated_tiles()
        filtered_targets = list(filter(lambda x: self.in_structure_range(x), unpopulated_squares))
        found = grid.get_random_grid_square(squares=filtered_targets)
        return found

    def get_nearest_cultivable(self):
        unpopulated_squares = grid.get_unpopulated_tiles()
        filtered_targets = list(filter(lambda x: self.in_structure_range(x), unpopulated_squares))
        closest = movement.get_closest_target(self, filtered_targets)
        return closest

    def get_nearest_storage(self):
        kind = ''
        for key, value in self.inventory.contents.items():
            if value > 0:
                kind = key
                break
        filtered_storages = list(filter(lambda x: x.output_inventory.has_room(kind), config.game.storage_structures))
        if not filtered_storages:
            return None
        target = movement.get_closest_target(self, filtered_storages)
        return target

    def get_nearest_storage_with(self, kind: str):
        # ensures list has at least one element
        if not kind:
            return None
        filtered_storages = list(filter(lambda x: x.check_inventory_for(kind), config.game.storage_structures))
        if not filtered_storages:
            return None
        target = movement.get_closest_target(self, filtered_storages)
        return target

    def check_target_nearby(self):
        if not self.target:
            return False
        collided = pygame.sprite.collide_rect(self, self.target)
        if collided:
            if self.target.label == 'dummy':
                self.target.expired = True
            return True

    def in_structure_range(self, structure):
        # default to True, so non-restricted roles like porter don't have a range limitation
        in_range = True
        if self.assignment and self.assignment.influence.range > 1:
            in_range = self.assignment.influence.rect.colliderect(structure.rect)
        return in_range

    def check_for_obstacles(self):
        if not self.target:
            return
        completed = False
        current_start = self.rect.center
        self.path_line_segments = [current_start]
        self.path_line_segments.append(self.target.rect.center)
        return
        # temporary; reinstate below to handle multiple collision points

        actual_end = self.target.rect.center
        adjusted_end = actual_end
        count = 0
        while count < 100:
            for obstacle in config.game.blocking_sprites:
                clipped = obstacle.rect.clipline(current_start, adjusted_end)
                if clipped:
                    xs, ys = current_start
                    x1, y1 = clipped[0]
                    x2, y2 = clipped[1]
                    # move clip position away from clip rect
                    x1 += (5 if x1 < xs else -5)
                    y1 += (5 if y1 < ys else -5)
                    # calculate line perpendicular to clip line through rect
                    # this is a line from origin
                    perp = (-y2 + y1, x2 - x1)
                    # offset perpendicular line to clip position
                    # and give it some breathing room
                    adjusted_end = (1.5 * perp[0] + x1, 1.5 * perp[1] + y1)

                    # add the clip point and the new end point to the line_segments list
                    self.path_line_segments.append((x1, y1))
                    self.path_line_segments.append(adjusted_end)
                    current_start = adjusted_end
                else:
                    continue
            count += 1
        self.path_line_segments.append(actual_end)


    #################################
    ##### REPR MANAGEMENT       #####
    #################################

    def update_repr(self):
        self.debug_repr_sprite.update(self.role)
        pos = (self.rect.x, self.rect.y - 10)
        size = (self.rect.width, self.rect.height)
        self.debug_repr_sprite.rect.update(pos, size)
        if not self.state:
            return
        if self.state.hungry:
            self.status_message = 'hungry'
        elif not self.target or self.target.target_type == 'dummy':
            self.status_message = 'idling'
        elif self.state.planting:
            planting_item = self.assignment.cultivate_data.get('name')
            self.status_message = f'going to plant: {planting_item}'
        elif self.state.store_at_storage or self.state.store_at_workable:
            self.status_message = f'delivering to {self.target.label}'
        elif self.state.find_product or self.state.find_resource:
            self.status_message = f'heading to {self.target.label}'
        elif self.state.find_in_storage:
            self.status_message = f'retrieving from {self.target.target_type}: {self.target.label}'

    def food_repr(self):
        return f'{self.food // 10}%'

    def inventory_repr(self):
        return self.inventory.get_contents_repr()

    def position_repr(self):
        return self.rect.center

    def role_repr(self):
        return self.role

    def status_repr(self):
        return self.status_message

    def target_repr(self):
        if not self.target:
            return ''
        return self.target.label

    def set_reprs(self):
        self.reprs = {
            'inventory': self.inventory_repr,
            'position': self.position_repr,
            'role': self.role_repr,
            'status': self.status_repr,
            'target': self.target_repr,
            'food': self.food_repr,
        }

    def show_context(self):
        self.context_pane.display()

    def update_status_text(self):
        if self.status_text_object is None:
            # Initialize new text

            # logger.info("Creating status text object")
            self.status_text_object = Text(
                    config.TINY_FONT,
                    colors.WHITE,
                    colors.TRANSPARENT,
                    self.rect.x + 25,
                    self.rect.y,
                    contents=self.status_message,
                )

        else:
            # Update location of text
            self.status_text_object.x = self.rect.x + 25
            self.status_text_object.y = self.rect.y
        self.status_text_object.update(self.status_message)

    def draw_line(self):
        for i, entry in enumerate(self.path_line_segments):
            try:
                first = self.path_line_segments[i]
                second = self.path_line_segments[i + 1]
            except IndexError:
                continue
            #pygame.draw.line(config.game.screen, colors.WHITE, first, second, width=1)
            pygame.draw.line(config.game.screen, colors.BLUE, first, second, width=1)
