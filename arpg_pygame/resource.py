import pygame
import random
import time

from arpg_pygame import colors
from arpg_pygame.context_pane import ContextPane
from arpg_pygame import movement
from arpg_pygame import polygons
from arpg_pygame.product import Product
from arpg_pygame.progress import Progress
from arpg_pygame.settings import config
from arpg_pygame.target import Target

class Resource(pygame.sprite.Sprite):
    def __init__(self, position, resource_type):
        super(Resource, self).__init__()
        self.position = position
        self.resource_type = resource_type
        self.label = resource_type
        self.size = 0
        self.target_size = 5 if self.resource_type == 'creature' else 1
        self.max_size = config.type_sizes.get(self.resource_type)
        self.surf, self.rect = self.generate_surface()
        self.sprite_type = 'resource'
        self.target_type = 'resource'
        self.active = False
        self.claimed = False
        self.expired = False
        self.start_time = time.time()
        self.progress = 0
        self.progress_display = Progress(self)
        self.complete_threshold = 100
        self.set_grow_time()
        self.show = True
        self.set_reprs()
        self.context_pane = ContextPane(self, self.sprite_type)

    def generate_surface(self):
        side = config.growth_sizes[self.size]
        # SRCALPHA makes it transparent; this is the surface the polygon will get drawn on
        surf = pygame.Surface((side, side), pygame.SRCALPHA)

        polygons.draw_shape(surf, self)
        position = self.position
        if hasattr(self, 'rect'):
            position = self.rect.center
        rect = surf.get_rect(center=position)
        # want to return rather than setting via self dot-notation so we can clearly see the params in init
        return surf, rect

    def work(self):
        self.progress += 2
        completed = self.progress >= self.complete_threshold
        if completed:
            self.progress -= self.complete_threshold
            self.process_work_complete()
        return completed

    def update(self):
        self.context_pane.update()
        self.progress_display.update(self.progress)
        if self.resource_type == 'creature':
            self.manage_target()
            movement.move_toward_target(self, self.target.rect)
        if self.active:
            return

        if self.size < self.max_size:
            if time.time() - self.start_time > self.grow_time:
                self.grow()
        else:
            self.activate()

    def manage_target(self):
        """
        for living resources
        """
        if not hasattr(self, 'target') or self.check_reached_target():
            self.target = Target(colors.get_random_color())

    def check_reached_target(self):
        """
        for living resources
        """
        collided = pygame.sprite.collide_rect(self, self.target)
        return collided

    def draw(self, screen, camera=None):
        screen.blit(self.progress_display.surf, self.progress_display.rect if not camera else camera.apply(self.progress_display))

    def grow(self):
        self.set_grow_time()
        self.surf, self.rect = self.generate_surface()
        self.size += 1
        self.set_grow_time()
        self.start_time = time.time()

    def set_grow_time(self):
        self.grow_time = random.randrange(config.growth_rate.get('min'), config.growth_rate.get('max'))

    def activate(self):
        self.active = True

    def destroy(self):
        self.active = False
        self.claimed = False
        self.expired = True
        self.kill()

    def process_work_complete(self):
        # attrs can be custom
        # 3 params needed: object to create, where to put it, how many to create
        # quantity would vary with type of resource/building being worked
        #position = (self.rect.center[0], self.rect.center[1])
        # create the event with ADDPRODUCT ID
        #product_type = config.resource_product_types[self.resource_type]
        products = config.resource_product_types.get(self.resource_type)
        for key, value in products.items():
            make_thing = pygame.event.Event(
                    config.game.events.ADDPRODUCT,
                    product=Product,
                    product_type=key,
                    position=self.rect.center,
                    quantity=value
            )
            # post event to event queue
            pygame.event.post(make_thing)

    def show_context(self):
        self.context_pane.display()

    def label_repr(self):
        return self.label

    def position_repr(self):
        return self.rect.center

    def progress_repr(self):
        return f'{self.progress}%'

    def claimed_repr(self):
        return "TRUE" if self.claimed else "FALSE"

    def set_reprs(self):
        self.reprs = {
            'label': self.label_repr,
            'position': self.position_repr,
            'progress': self.progress_repr,
            'claimed': self.claimed_repr,
        }
