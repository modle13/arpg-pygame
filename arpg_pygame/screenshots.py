import logging
import os
import pygame
import re

from arpg_pygame.settings import config

logger = logging.getLogger(__name__)

def save_screenshot():
    folder = os.path.join(config.game.HERE, 'screenshots')
    if not os.path.exists(folder):
        os.makedirs(folder)

    index = 0
    for f in os.listdir(folder):
        # match filenames that come from this app
        match = re.match('screenshot\d+.png', f)
        if match:
            # if a file exists,
            # check if that file's index is bigger than
            # the current index;
            # indices are formatted with 3 digits
            new_index = int(re.search('\d+', f).group())
            index = max(index, new_index + 1)

    screenshot_name = f'screenshot{index:03d}.png'
    screenshot_path = os.path.join(folder, screenshot_name)
    pygame.image.save(config.game.screen, screenshot_path)
    logger.info(f'screenshot saved to {screenshot_path}')
