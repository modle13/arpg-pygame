import pygame
import random

from arpg_pygame.settings import config

class Target(pygame.sprite.Sprite):
    def __init__(self, color):
        super(Target, self).__init__()
        unit = config.tile_size
        self.surf = pygame.Surface((unit, unit))
        multiplier = unit
        # divide screen dimensions by multiplier to ensure
        # targets aren't too close together
        x = random.randrange(int(config.max_width / multiplier)) * multiplier
        y = random.randrange(int(config.max_height / multiplier)) * multiplier
        self.start = (x, y)
        self.rect = self.surf.get_rect(topleft=self.start)
        self.target_type = 'dummy'
        self.label = 'dummy'
        self.cultivator = False
        self.claimed = False
        self.active = True
        self.expired = False
        self.show = True
        self.surf.fill(color)
        self.target_size = 1

    def work(self):
        return True

    def __repr__(self):
        return 'dummy target'
