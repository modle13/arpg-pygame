import random

BLACK = (0, 0, 0)
BLUE = (0, 0, 128)
BROWN = (153, 76, 0)
GRAY = (100, 100, 100)
GREEN = (0, 255, 0)
LIGHT_BLUE = (173, 216, 230)
LIGHT_BLUE_TRANS = (173, 216, 230, 10)
LIGHT_BROWN = (181, 101, 29)
LIGHT_GREEN = (144, 238, 144)
LIGHT_RED = (255, 132, 132)
LIGHT_YELLOW = (255, 255, 153)
MAGENTA = (255, 0, 255)
OFF_WHITE = (251, 247, 245)
ORANGE = (255, 165, 0)
ORANGE_TRANS = (255, 165, 0, 64)
PINK = (255, 192, 203)
PINK_TRANS = (255, 192, 203, 64)
RED = (255, 0, 0)
RED_TRANS = (255, 0, 0, 64)
TAN = (210, 180, 140)
TRANSPARENT = (0, 0, 0, 0)
WHITE = (255, 255, 255)
YELLOW = (255, 255, 0)

def get_random_color():
    color = (random.randrange(120, 220), random.randrange(120, 220), random.randrange(120, 220))
    return color
