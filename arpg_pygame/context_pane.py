import logging
import pygame

from arpg_pygame import colors
from arpg_pygame.settings import config
from arpg_pygame.text import build_text_object

logger = logging.getLogger(__name__)

class Text(pygame.sprite.Sprite):
    def __init__(self, text, font, position, show=True, background=colors.BLACK):
        super(Text, self).__init__()
        self.surf, self.rect = build_text_object(text, font, bg_color=background)
        self.rect.center = position
        self.show = show


# extend Sprite to take advantage of sprite groups
class ContextPane(pygame.sprite.Sprite):
    def __init__(self, sprite, sprite_type, show=False):
        super(ContextPane, self).__init__()
        self.source_sprite = sprite
        self.source_sprite_type = sprite_type
        self.color = colors.GRAY
        # self.font is used for calculating pane width
        self.linesize = config.SMALL_FONT.get_linesize()
        self.max_width = 0
        self.show = show
        self.background_size = (1, 1)
        self.text_sprites = pygame.sprite.Group()
        self.build_sprites()
        self.set_background_size()
        self.surf = pygame.Surface(self.background_size)
        self.rect = self.surf.get_rect(center=config.SCREEN_CENTER)
    def build_sprites(self):
        self.text_sprites.empty()
        self.surf = pygame.Surface(self.background_size)
        self.surf.fill(self.color)
        context_def = config.sprite_context_defs.get(self.source_sprite_type)
        font = context_def.get('font')
        for field in context_def.get('fields'):
            entry_repr_func = self.source_sprite.reprs.get(field)
            if not entry_repr_func:
                continue
            entry_repr = entry_repr_func()
            if isinstance(entry_repr, list):
                field = 'inventory' if field == 'output' and 'input' not in self.source_sprite.reprs else field
                self.make_sprite(f' ', font)
                self.make_sprite(field.upper(), font)
                for entry in entry_repr:
                    self.make_sprite(entry, font)
            else:
                self.make_sprite(entry_repr, font, prefix=field)
    def make_sprite(self, text, font, prefix=''):
        x, y = self.background_size
        if prefix:
            text = f'{prefix}: {text}'
        text_width, _ = config.SMALL_FONT.size(text)
        if text_width > self.max_width:
            self.max_width = text_width
        inner_text_position = (x // 2, y // 8 + self.linesize * len(self.text_sprites))
        text_sprite = Text(text, font, inner_text_position, background=self.color)
        self.set_screen_position(text_width)
        self.text_sprites.add(text_sprite)
    def set_background_size(self):
        if hasattr(self, 'text_sprites'):
            self.background_size = (int(self.max_width), (2 + len(self.text_sprites)) * self.linesize)
    def set_screen_position(self, text_width):
        x, y = self.background_size
        self.rect = self.surf.get_rect(
            topleft=(
                config.control_panel_info_popup_pos[0],
                config.control_panel_vertical_pos,
            )
        )
    def update(self):
        self.set_text_sprite_visibility()
        if not self.show:
            return
        self.set_background_size()
        self.build_sprites()
    # don't like having to pass the kwarg here, but may need context panes to follow camera?
    def draw(self, screen, camera=None):
        if not self.show:
            return
        for entry in self.text_sprites:
            self.surf.blit(entry.surf, entry.rect)
        config.game.screen.blit(self.surf, self.rect)
    def toggle(self):
        self.show = not self.show
        self.set_text_sprite_visibility()
    def hide(self):
        self.show = False
        self.set_text_sprite_visibility()
    def display(self):
        self.show = True
        # without update call here, things like resources may display properly
        self.update()
    def set_text_sprite_visibility(self):
        for entry in self.text_sprites:
            entry.show = self.show
