import pygame

from arpg_pygame.settings import config

class Tile(pygame.sprite.Sprite):
    def __init__(self, start, color):
        super(Tile, self).__init__()
        #self.surf = pygame.Surface((config.tile_size, config.tile_size))
        #self.surf = pygame.Surface((config.tile_size * 1.2, config.tile_size * 1.2))
        self.surf = pygame.Surface((config.tile_size / 5, config.tile_size / 5))
        self.surf.fill(color)
        self.rect = self.surf.get_rect(center=start)
        self.show = True
