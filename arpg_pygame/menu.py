import logging
import pygame

from arpg_pygame.settings import config
from arpg_pygame.text import build_text_object

logger = logging.getLogger(__name__)

class Text(pygame.sprite.Sprite):
    def __init__(self, text, font, position, show=True):
        super(Text, self).__init__()
        self.surf, self.rect = build_text_object(text, font)
        self.rect.center = position
        self.show = show


# extend Sprite to take advantage of sprite groups
class Menu(pygame.sprite.Sprite):
    def __init__(self, menu_type, show=True):
        super(Menu, self).__init__()
        self.menu_type = menu_type
        self.label = menu_type
        self.show = show
        self.menu_defs = config.menu_defs.get(menu_type)
        self.text_sprites = pygame.sprite.Group()
        self.build_sprites()
        self.surf = pygame.Surface(self.menu_defs.get('background_size'))
        self.rect = self.surf.get_rect(center=config.SCREEN_CENTER)
    def build_sprites(self):
        for entry in self.menu_defs.get('entries'):
            sprite = Text(entry.get('text'), entry.get('font'), entry.get('position'))
            self.text_sprites.add(sprite)
    def draw(self):
        for entry in self.text_sprites:
            self.surf.blit(entry.surf, entry.rect)
        config.game.screen.blit(self.surf, self.rect)
    def toggle(self):
        self.show = not self.show
        for entry in self.text_sprites:
            entry.show = self.show
