import math
import pygame

from arpg_pygame import colors
from arpg_pygame import keys
from arpg_pygame.settings import config
from arpg_pygame import camera


can_move_up    = lambda value: value > 1
can_move_down  = lambda value: value < config.max_height
can_move_left  = lambda value: value > 1
can_move_right = lambda value: value < config.max_width

directions = {
    'UP':    {'axis': 'y', 'value': -1, 'check': can_move_up   },
    'DOWN':  {'axis': 'y', 'value':  1, 'check': can_move_down },
    'LEFT':  {'axis': 'x', 'value': -1, 'check': can_move_left },
    'RIGHT': {'axis': 'x', 'value':  1, 'check': can_move_right},
}


class AttackAnimation(pygame.sprite.Sprite):
    def __init__(self):
        print('this is an attack animation sprite')
        #print(pygame.time.get_ticks())
        # slim rectangle; change to sword polygon later
        self.surf = pygame.Surface((5, 5))
        self.surf.fill(colors.WHITE)
        self.rect = self.surf.get_rect()
        self.show = True


class Reticle(pygame.sprite.Sprite):
    def __init__(self):
        super(Reticle, self).__init__()
        # center of game world
        self.start = (config.max_width / 2 + config.grid_square_size, config.max_height / 2 + config.grid_square_size)
        # top-left of game world
        #self.start = (config.HALF_WIDTH, config.HALF_HEIGHT)
        self.surf = pygame.Surface((10, 10))
        self.surf.fill(colors.WHITE)
        self.rect = self.surf.get_rect(center=self.start)
        self.block_size = config.grid_square_size
        self.mouse_offset = None
        self.original_pos_on_mouse_down = None
        self.attack_animation = AttackAnimation()
        self.show = True
        self.rotation = 0
        self.rotation_speed = 2

    def update(self):
        """Move the position based on user keypresses."""
        pressed_keys = config.game.pressed_keys
        self.animate_swing()

        # not using elif here so that each direction processes each update
        # otherwise higher priority checks would override lower, and diagonal movement would not work
        if pressed_keys[keys.K_UP]    or pressed_keys[keys.K_w]:
            self.move('UP', self.rect.y)
        if pressed_keys[keys.K_DOWN]  or pressed_keys[keys.K_s]:
            self.move('DOWN', self.rect.y)
        if pressed_keys[keys.K_LEFT]  or pressed_keys[keys.K_a]:
            self.move('LEFT', self.rect.x)
        if pressed_keys[keys.K_RIGHT] or pressed_keys[keys.K_d]:
            self.move('RIGHT', self.rect.x)

        self.sync_attack_sprite_positions()

    def animate_swing(self):
        pass

    def sync_attack_sprite_positions(self):
        self.attack_animation.rect.x = self.rect.x + 15
        self.attack_animation.rect.y = self.rect.y

    def move(self, direction, check_param):
        direction_details = directions.get(direction)
        if not direction_details.get('check')(check_param):
            return
        axis = direction_details.get('axis')
        dir_value = direction_details.get('value')
        speed = config.unit / 10
        if axis == 'y':
            self.rect.y += dir_value * speed
        elif axis == 'x':
            self.rect.x += dir_value * speed

    def set_mouse_offset(self, pos: tuple):
        self.original_mouse_pos = pos
        curr_x, curr_y, _, _ = self.rect
        self.original_pos_on_mouse_down = (curr_x, curr_y)
        self.mouse_drag = True

    def clear_mouse_offset(self):
        self.mouse_drag = False

    def draw(self, screen, camera=None):
        # preserve center of old rect
        old_center = self.attack_animation.rect.center
        # determine rotation
        self.rotation = (self.rotation + self.rotation_speed) % 360
        # rotate image
        new_image = pygame.transform.rotate(self.attack_animation.surf, self.rotation)
        # determine new rect
        rect = new_image.get_rect(center=old_center)

        screen.blit(new_image, config.camera.apply(rect))
