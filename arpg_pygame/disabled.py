import pygame

from arpg_pygame.settings import config
from arpg_pygame import colors
from arpg_pygame import polygons


# move this to structure file like Influence?
class Disabled(pygame.sprite.Sprite):
    def __init__(self, parent_sprite=None, show=False):
        self.kind = 'disabled'
        self.unit = config.grid_square_size / 2
        self.parent_sprite = parent_sprite
        # needed to get type size of shape
        self.resource_type = 'disabled'
        self.show = show
        self.size = 4
        self.surf, self.rect = self.generate_sprite()
    def generate_sprite(self):
        surf = pygame.Surface((self.unit, self.unit), pygame.SRCALPHA)
        if self.parent_sprite:
            rect = surf.get_rect(topleft=self.parent_sprite.rect.topleft)
        else:
            rect = surf.get_rect(topleft=(self.x, self.y))
        polygons.draw_shape(surf, self)
        return surf, rect
