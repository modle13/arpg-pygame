#!/usr/bin/env python

"""
adapted from https://realpython.com/pygame-a-primer
"""

import pygame
import random

# Import pygame.locals for easier access to key coordinates
from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_w,
    K_a,
    K_s,
    K_d,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

# Define constants for the screen width and height
SCREEN_WIDTH = 1920
SCREEN_HEIGHT = 1080


class Game():
    def __init__(self):
        # Setup the clock for a decent framerate
        self.clock = pygame.time.Clock()
        # Create the screen object
        self.screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
        # Create groups to hold enemy sprites and all sprites
        # - all_sprites is used for rendering
        self.all_sprites = pygame.sprite.Group()
        # - enemies is used for collision detection and position updates
        self.enemies = pygame.sprite.Group()
        self.clouds = pygame.sprite.Group()
        self.assets = AssetBucket()
        self.events = Events()
        self.player = Player()
        self.all_sprites.add(self.player)

    def update_frame(self):
        pygame.display.flip()
        self.clock.tick(30)

    def run(self):
        self.events.update()
        # Get the set of keys pressed and check for user input
        pressed_keys = pygame.key.get_pressed()
        # Update the player sprite based on user keypresses
        self.player.update(pressed_keys)
        # Update the position of enemies and clouds
        self.enemies.update()
        self.clouds.update()
    
        self.draw()

        self.process_collisions()

        self.update_frame()

    def process_collisions(self):
        # Check if any enemies have collided with the player
        if pygame.sprite.spritecollideany(self.player, self.enemies):
            # If so, then remove the player and stop the loop
            self.assets.move_up_sound.stop()
            self.assets.move_down_sound.stop()
            self.assets.collision_sound.play()
            self.player.kill()
            self.running = False
            pygame.time.wait(3000)

    def draw(self):
        # Fill the screen with black
        self.screen.fill((0, 0, 0))
        # Draw all sprites
        for entity in self.all_sprites:
            self.screen.blit(entity.surf, entity.rect)

    def quit(self):
        pygame.mixer.music.stop()
        pygame.mixer.quit()


class AssetBucket():
    def __init__(self):
        # Load and play background music
        #pygame.mixer.music.load("assets/themusic.mp3")
        #pygame.mixer.music.play(loops=-1)

        # Load all sound files
        # Sound sources: Jon Fincher
        self.move_up_sound = pygame.mixer.Sound("assets/Rising_putter.ogg")
        self.move_down_sound = pygame.mixer.Sound("assets/Falling_putter.ogg")
        self.collision_sound = pygame.mixer.Sound("assets/Collision.ogg")


class Events():
    def __init__(self):
        # Create a custom event for adding a new enemy
        self.ADDENEMY = pygame.USEREVENT + 1
        pygame.time.set_timer(self.ADDENEMY, 250)
        self.ADDCLOUD = pygame.USEREVENT + 2
        pygame.time.set_timer(self.ADDCLOUD, 1000)
    def update(self):
        # Look at every event in the queue
        for event in pygame.event.get():
            # Did the user hit the escape key?
            if event.type == KEYDOWN and event.key == K_ESCAPE:
                game.running = False

            # Did the user click the window close button? If so, stop the loop.
            elif event.type == QUIT:
                game.running = False

            # Add a new enemy?
            elif event.type == self.ADDENEMY:
                # Create the new enemy and add it to sprite groups
                new_enemy = Enemy()
                game.enemies.add(new_enemy)
                game.all_sprites.add(new_enemy)

            # Add a new cloud?
            elif event.type == self.ADDCLOUD:
                # Create the new cloud and add it to sprite groups
                new_cloud = Cloud()
                game.clouds.add(new_cloud)
                game.all_sprites.add(new_cloud)


# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.image.load("assets/player.png").convert()
        self.surf.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.surf.get_rect()
        self.speed = 30

    # Move the sprite based on user keypresses
    def update(self, pressed_keys):
        if pressed_keys[K_UP] or pressed_keys[K_w]:
            self.rect.move_ip(0, -self.speed)
            game.assets.move_up_sound.play()
        if pressed_keys[K_DOWN] or pressed_keys[K_s]:
            self.rect.move_ip(0, self.speed)
            game.assets.move_down_sound.play()
        if pressed_keys[K_LEFT] or pressed_keys[K_a]:
            self.rect.move_ip(-self.speed, 0)
        if pressed_keys[K_RIGHT] or pressed_keys[K_d]:
            self.rect.move_ip(self.speed, 0)
        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT


# Define the enemy object by extending pygame.sprite.Sprite
# The surface you draw on the screen is now an attribute of 'enemy'
class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super(Enemy, self).__init__()
        self.surf = pygame.image.load("assets/missile.png").convert()
        self.surf.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
            )
        )
        self.speed = random.randint(5, 20)

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen
    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()


# Define the cloud object by extending pygame.sprite.Sprite
# Use an image for a better-looking sprite
class Cloud(pygame.sprite.Sprite):
    def __init__(self):
        super(Cloud, self).__init__()
        self.surf = pygame.image.load("assets/cloud.png").convert()
        self.surf.set_colorkey((0, 0, 0), RLEACCEL)
        # The starting position is randomly generated
        self.rect = self.surf.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
            )
        )

    # Move the cloud based on a constant speed
    # Remove the cloud when it passes the left edge of the screen
    def update(self):
        self.rect.move_ip(-5, 0)
        if self.rect.right < 0:
            self.kill()        


def init_game():
    # Setup for sounds. Defaults are good.
    pygame.mixer.init()

    # Initialize pygame
    pygame.init()

    game = Game()
    return game


if __name__ == '__main__':
    game = init_game()
    # Variable to keep the main loop running
    game.running = True
    # Main loop
    while game.running:
        game.run()
    game.quit()
